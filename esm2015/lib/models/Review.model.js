import { MessageBaseModel, MessageSource } from './Message.model';
export class ReviewsModel {
    constructor(data) {
        this.averageRating = data.averageRating;
        this.reviews = data.reviews;
        this.totalReviewCount = data.totalReviewCount;
    }
    static fromApiResponse(data) {
        const reviewList = (!Array.isArray(data.reviews) ? [] : data.reviews).map(review => ReviewModel.fromApiResponse({
            id: review.id,
            comment: review.comment,
            createTime: review.createTime,
            reviewer: review.reviewer,
            starRating: review.starRating,
            updateTime: review.updateTime,
            reviewReply: review.reviewReply
        }));
        return new ReviewsModel({
            averageRating: data.averageRating || 0,
            reviews: reviewList,
            totalReviewCount: data.totalReviewCount || 0
        });
    }
    empty() {
        return new ReviewsModel({
            averageRating: null,
            reviews: null,
            totalReviewCount: null
        });
    }
}
export class ReviewModel extends MessageBaseModel {
    constructor(data) {
        super({
            id: data.id,
            senderName: data.senderName,
            message: data.message,
            createTime: data.createTime,
            source: data.source
        });
        this.senderPhotoUrl = data.senderPhotoUrl;
        this.starRating = data.starRating;
        this.updateTime = data.updateTime;
        this.reviewReply = data.reviewReply;
    }
    static fromApiResponse(data) {
        return new ReviewModel({
            // base class properties:
            id: data.id,
            senderName: data.reviewer.displayName,
            message: data.comment,
            createTime: data.createTime,
            source: MessageSource.GMB_REVIEW,
            // own (extended) properties:
            senderPhotoUrl: data.reviewer.profilePhotoUrl,
            starRating: data.starRating,
            updateTime: data.updateTime,
            reviewReply: data.reviewReply
        });
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUmV2aWV3Lm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGJveHgvbWVzc2FnZXMtY29yZS8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbHMvUmV2aWV3Lm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBcUIsZ0JBQWdCLEVBQUUsYUFBYSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFTckYsTUFBTSxPQUFPLFlBQVk7SUFDckIsWUFBWSxJQUF3QjtRQUNoQyxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUM7UUFDeEMsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBQzVCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUM7SUFDbEQsQ0FBQztJQU1ELE1BQU0sQ0FBQyxlQUFlLENBQUMsSUFBaUI7UUFDcEMsTUFBTSxVQUFVLEdBQWtCLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsR0FBRyxDQUNwRixNQUFNLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUM7WUFDbEMsRUFBRSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ2IsT0FBTyxFQUFFLE1BQU0sQ0FBQyxPQUFPO1lBQ3ZCLFVBQVUsRUFBRSxNQUFNLENBQUMsVUFBVTtZQUM3QixRQUFRLEVBQUUsTUFBTSxDQUFDLFFBQVE7WUFDekIsVUFBVSxFQUFFLE1BQU0sQ0FBQyxVQUFVO1lBQzdCLFVBQVUsRUFBRSxNQUFNLENBQUMsVUFBVTtZQUM3QixXQUFXLEVBQUUsTUFBTSxDQUFDLFdBQVc7U0FDbEMsQ0FBQyxDQUNMLENBQUM7UUFFRixPQUFPLElBQUksWUFBWSxDQUFDO1lBQ3BCLGFBQWEsRUFBRSxJQUFJLENBQUMsYUFBYSxJQUFJLENBQUM7WUFDdEMsT0FBTyxFQUFFLFVBQVU7WUFDbkIsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixJQUFJLENBQUM7U0FDL0MsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELEtBQUs7UUFDRCxPQUFPLElBQUksWUFBWSxDQUFDO1lBQ3BCLGFBQWEsRUFBRSxJQUFJO1lBQ25CLE9BQU8sRUFBRSxJQUFJO1lBQ2IsZ0JBQWdCLEVBQUUsSUFBSTtTQUN6QixDQUFDLENBQUM7SUFDUCxDQUFDO0NBQ0o7QUFTRCxNQUFNLE9BQU8sV0FBWSxTQUFRLGdCQUFnQjtJQUM3QyxZQUFZLElBQXNCO1FBQzlCLEtBQUssQ0FBQztZQUNGLEVBQUUsRUFBRSxJQUFJLENBQUMsRUFBRTtZQUNYLFVBQVUsRUFBRSxJQUFJLENBQUMsVUFBVTtZQUMzQixPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU87WUFDckIsVUFBVSxFQUFFLElBQUksQ0FBQyxVQUFVO1lBQzNCLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTTtTQUN0QixDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUM7UUFDMUMsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO1FBQ2xDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQztRQUNsQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7SUFDeEMsQ0FBQztJQU9ELE1BQU0sQ0FBQyxlQUFlLENBQUMsSUFBZ0I7UUFDbkMsT0FBTyxJQUFJLFdBQVcsQ0FBQztZQUNuQix5QkFBeUI7WUFDekIsRUFBRSxFQUFFLElBQUksQ0FBQyxFQUFFO1lBQ1gsVUFBVSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVztZQUNyQyxPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU87WUFDckIsVUFBVSxFQUFFLElBQUksQ0FBQyxVQUFVO1lBQzNCLE1BQU0sRUFBRSxhQUFhLENBQUMsVUFBVTtZQUVoQyw2QkFBNkI7WUFDN0IsY0FBYyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsZUFBZTtZQUM3QyxVQUFVLEVBQUUsSUFBSSxDQUFDLFVBQVU7WUFDM0IsVUFBVSxFQUFFLElBQUksQ0FBQyxVQUFVO1lBQzNCLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVztTQUNoQyxDQUFDLENBQUM7SUFDUCxDQUFDO0NBQ0oiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJTWVzc2FnZUJhc2VQcm9wcywgTWVzc2FnZUJhc2VNb2RlbCwgTWVzc2FnZVNvdXJjZSB9IGZyb20gJy4vTWVzc2FnZS5tb2RlbCc7XG5pbXBvcnQgeyBJUmV2aWV3QXBpLCBJUmV2aWV3UmVwbHlBcGksIElSZXZpZXdzQXBpIH0gZnJvbSAnLi9SZXZpZXdzLWFwaS5wcm9wZXJ0aWVzJztcblxuZXhwb3J0IGludGVyZmFjZSBJUmV2aWV3c01vZGVsUHJvcHMge1xuICAgIGF2ZXJhZ2VSYXRpbmc6IG51bWJlcjtcbiAgICByZXZpZXdzOiBSZXZpZXdNb2RlbFtdO1xuICAgIHRvdGFsUmV2aWV3Q291bnQ6IG51bWJlcjtcbn1cblxuZXhwb3J0IGNsYXNzIFJldmlld3NNb2RlbCBpbXBsZW1lbnRzIElSZXZpZXdzTW9kZWxQcm9wcyB7XG4gICAgY29uc3RydWN0b3IoZGF0YTogSVJldmlld3NNb2RlbFByb3BzKSB7XG4gICAgICAgIHRoaXMuYXZlcmFnZVJhdGluZyA9IGRhdGEuYXZlcmFnZVJhdGluZztcbiAgICAgICAgdGhpcy5yZXZpZXdzID0gZGF0YS5yZXZpZXdzO1xuICAgICAgICB0aGlzLnRvdGFsUmV2aWV3Q291bnQgPSBkYXRhLnRvdGFsUmV2aWV3Q291bnQ7XG4gICAgfVxuXG4gICAgYXZlcmFnZVJhdGluZzogbnVtYmVyO1xuICAgIHJldmlld3M6IFJldmlld01vZGVsW107XG4gICAgdG90YWxSZXZpZXdDb3VudDogbnVtYmVyO1xuXG4gICAgc3RhdGljIGZyb21BcGlSZXNwb25zZShkYXRhOiBJUmV2aWV3c0FwaSkge1xuICAgICAgICBjb25zdCByZXZpZXdMaXN0OiBSZXZpZXdNb2RlbFtdID0gKCFBcnJheS5pc0FycmF5KGRhdGEucmV2aWV3cykgPyBbXSA6IGRhdGEucmV2aWV3cykubWFwKFxuICAgICAgICAgICAgcmV2aWV3ID0+IFJldmlld01vZGVsLmZyb21BcGlSZXNwb25zZSh7XG4gICAgICAgICAgICAgICAgaWQ6IHJldmlldy5pZCxcbiAgICAgICAgICAgICAgICBjb21tZW50OiByZXZpZXcuY29tbWVudCxcbiAgICAgICAgICAgICAgICBjcmVhdGVUaW1lOiByZXZpZXcuY3JlYXRlVGltZSxcbiAgICAgICAgICAgICAgICByZXZpZXdlcjogcmV2aWV3LnJldmlld2VyLFxuICAgICAgICAgICAgICAgIHN0YXJSYXRpbmc6IHJldmlldy5zdGFyUmF0aW5nLFxuICAgICAgICAgICAgICAgIHVwZGF0ZVRpbWU6IHJldmlldy51cGRhdGVUaW1lLFxuICAgICAgICAgICAgICAgIHJldmlld1JlcGx5OiByZXZpZXcucmV2aWV3UmVwbHlcbiAgICAgICAgICAgIH0pXG4gICAgICAgICk7XG5cbiAgICAgICAgcmV0dXJuIG5ldyBSZXZpZXdzTW9kZWwoe1xuICAgICAgICAgICAgYXZlcmFnZVJhdGluZzogZGF0YS5hdmVyYWdlUmF0aW5nIHx8IDAsXG4gICAgICAgICAgICByZXZpZXdzOiByZXZpZXdMaXN0LFxuICAgICAgICAgICAgdG90YWxSZXZpZXdDb3VudDogZGF0YS50b3RhbFJldmlld0NvdW50IHx8IDBcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZW1wdHkoKSB7XG4gICAgICAgIHJldHVybiBuZXcgUmV2aWV3c01vZGVsKHtcbiAgICAgICAgICAgIGF2ZXJhZ2VSYXRpbmc6IG51bGwsXG4gICAgICAgICAgICByZXZpZXdzOiBudWxsLFxuICAgICAgICAgICAgdG90YWxSZXZpZXdDb3VudDogbnVsbFxuICAgICAgICB9KTtcbiAgICB9XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgUmV2aWV3TW9kZWxQcm9wcyBleHRlbmRzIElNZXNzYWdlQmFzZVByb3BzIHtcbiAgICBzZW5kZXJQaG90b1VybDogc3RyaW5nO1xuICAgIHN0YXJSYXRpbmc6IG51bWJlcjtcbiAgICB1cGRhdGVUaW1lOiBzdHJpbmc7XG4gICAgcmV2aWV3UmVwbHk/OiBJUmV2aWV3UmVwbHlBcGk7XG59XG5cbmV4cG9ydCBjbGFzcyBSZXZpZXdNb2RlbCBleHRlbmRzIE1lc3NhZ2VCYXNlTW9kZWwgaW1wbGVtZW50cyBSZXZpZXdNb2RlbFByb3BzIHtcbiAgICBjb25zdHJ1Y3RvcihkYXRhOiBSZXZpZXdNb2RlbFByb3BzKSB7XG4gICAgICAgIHN1cGVyKHtcbiAgICAgICAgICAgIGlkOiBkYXRhLmlkLFxuICAgICAgICAgICAgc2VuZGVyTmFtZTogZGF0YS5zZW5kZXJOYW1lLFxuICAgICAgICAgICAgbWVzc2FnZTogZGF0YS5tZXNzYWdlLFxuICAgICAgICAgICAgY3JlYXRlVGltZTogZGF0YS5jcmVhdGVUaW1lLFxuICAgICAgICAgICAgc291cmNlOiBkYXRhLnNvdXJjZVxuICAgICAgICB9KTtcblxuICAgICAgICB0aGlzLnNlbmRlclBob3RvVXJsID0gZGF0YS5zZW5kZXJQaG90b1VybDtcbiAgICAgICAgdGhpcy5zdGFyUmF0aW5nID0gZGF0YS5zdGFyUmF0aW5nO1xuICAgICAgICB0aGlzLnVwZGF0ZVRpbWUgPSBkYXRhLnVwZGF0ZVRpbWU7XG4gICAgICAgIHRoaXMucmV2aWV3UmVwbHkgPSBkYXRhLnJldmlld1JlcGx5O1xuICAgIH1cblxuICAgIHNlbmRlclBob3RvVXJsOiBzdHJpbmc7XG4gICAgc3RhclJhdGluZzogbnVtYmVyO1xuICAgIHVwZGF0ZVRpbWU6IHN0cmluZztcbiAgICByZXZpZXdSZXBseT86IElSZXZpZXdSZXBseUFwaTtcblxuICAgIHN0YXRpYyBmcm9tQXBpUmVzcG9uc2UoZGF0YTogSVJldmlld0FwaSk6IFJldmlld01vZGVsIHtcbiAgICAgICAgcmV0dXJuIG5ldyBSZXZpZXdNb2RlbCh7XG4gICAgICAgICAgICAvLyBiYXNlIGNsYXNzIHByb3BlcnRpZXM6XG4gICAgICAgICAgICBpZDogZGF0YS5pZCxcbiAgICAgICAgICAgIHNlbmRlck5hbWU6IGRhdGEucmV2aWV3ZXIuZGlzcGxheU5hbWUsXG4gICAgICAgICAgICBtZXNzYWdlOiBkYXRhLmNvbW1lbnQsXG4gICAgICAgICAgICBjcmVhdGVUaW1lOiBkYXRhLmNyZWF0ZVRpbWUsXG4gICAgICAgICAgICBzb3VyY2U6IE1lc3NhZ2VTb3VyY2UuR01CX1JFVklFVyxcblxuICAgICAgICAgICAgLy8gb3duIChleHRlbmRlZCkgcHJvcGVydGllczpcbiAgICAgICAgICAgIHNlbmRlclBob3RvVXJsOiBkYXRhLnJldmlld2VyLnByb2ZpbGVQaG90b1VybCxcbiAgICAgICAgICAgIHN0YXJSYXRpbmc6IGRhdGEuc3RhclJhdGluZyxcbiAgICAgICAgICAgIHVwZGF0ZVRpbWU6IGRhdGEudXBkYXRlVGltZSxcbiAgICAgICAgICAgIHJldmlld1JlcGx5OiBkYXRhLnJldmlld1JlcGx5XG4gICAgICAgIH0pO1xuICAgIH1cbn1cbiJdfQ==