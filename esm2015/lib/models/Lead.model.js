import { MessageBaseModel, MessageSource } from './Message.model';
export class LeadsModel {
    constructor(data) {
        this.totals = data.totals;
        this.periods = data.periods;
        this.leads = data.leads;
        this.chart = data.chart;
    }
    static fromApiResponse(data) {
        const leads = data.messages.map(lead => LeadModel.fromApiResponse(lead));
        return new LeadsModel({
            totals: data.totals,
            periods: data.periods,
            leads,
            chart: data.chart
        });
    }
    static empty() {
        return new LeadsModel({
            totals: {
                total: 0,
                seen: 0,
                new: 0
            },
            periods: null,
            leads: [],
            chart: []
        });
    }
}
export class LeadModel extends MessageBaseModel {
    constructor(data) {
        super({
            id: data.id,
            senderName: data.senderName,
            message: data.message,
            createTime: data.createTime,
            source: data.source
        });
        this.email = data.email;
        this.phone = data.phone;
        this.formattedDate = data.formattedDate;
        this.date = data.date;
        this.hour = data.hour;
        this.timeAgo = data.timeAgo;
        this.leadTimeAgo = data.leadTimeAgo;
        this.readStatus = data.readStatus;
        const shortMsg = (data.shortMessage || data.message.substring(0, 30));
        this.shortMessage = shortMsg.length >= 30 ? shortMsg.concat('...') : shortMsg;
        this.source = data.source;
    }
    static fromApiResponse(data) {
        return new LeadModel({
            id: +data.id,
            senderName: data.name || data.data_name,
            message: data.message || data.data_message,
            email: data.mail || data.data_email,
            phone: data.phone || data.data_phone,
            createTime: data.timestamp,
            formattedDate: data.formatted_date,
            date: data.date || data.created_at,
            hour: data.hour,
            timeAgo: data.time_ago,
            leadTimeAgo: data.lead_time_ago,
            readStatus: data.read_status === '1',
            shortMessage: data.short_message,
            source: MessageSource.EMAIL
        });
    }
    static empty() {
        return new LeadModel({
            id: null,
            senderName: '',
            createTime: null,
            message: '',
            email: null,
            phone: null,
            formattedDate: null,
            date: null,
            hour: null,
            timeAgo: null,
            leadTimeAgo: null,
            readStatus: null,
            shortMessage: null,
            source: MessageSource.UNKNOWN,
            favorite: false
        });
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTGVhZC5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bib3h4L21lc3NhZ2VzLWNvcmUvIiwic291cmNlcyI6WyJsaWIvbW9kZWxzL0xlYWQubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxFQUFxQixnQkFBZ0IsRUFBRSxhQUFhLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQVNyRixNQUFNLE9BQU8sVUFBVTtJQUNuQixZQUFZLElBQXFCO1FBQzdCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUMxQixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7UUFDNUIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQ3hCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztJQUM1QixDQUFDO0lBT0QsTUFBTSxDQUFDLGVBQWUsQ0FBQyxJQUFlO1FBQ2xDLE1BQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1FBRXpFLE9BQU8sSUFBSSxVQUFVLENBQUM7WUFDbEIsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNO1lBQ25CLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTztZQUNyQixLQUFLO1lBQ0wsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1NBQ3BCLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxNQUFNLENBQUMsS0FBSztRQUNSLE9BQU8sSUFBSSxVQUFVLENBQUM7WUFDbEIsTUFBTSxFQUFFO2dCQUNKLEtBQUssRUFBRSxDQUFDO2dCQUNSLElBQUksRUFBRSxDQUFDO2dCQUNQLEdBQUcsRUFBRSxDQUFDO2FBQ1Q7WUFDRCxPQUFPLEVBQUUsSUFBSTtZQUNiLEtBQUssRUFBRSxFQUFFO1lBQ1QsS0FBSyxFQUFFLEVBQUU7U0FDWixDQUFDLENBQUM7SUFDUCxDQUFDO0NBQ0o7QUFlRCxNQUFNLE9BQU8sU0FBVSxTQUFRLGdCQUFnQjtJQVkzQyxZQUFZLElBQXFCO1FBQzdCLEtBQUssQ0FBQztZQUNGLEVBQUUsRUFBRSxJQUFJLENBQUMsRUFBRTtZQUNYLFVBQVUsRUFBRSxJQUFJLENBQUMsVUFBVTtZQUMzQixPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU87WUFDckIsVUFBVSxFQUFFLElBQUksQ0FBQyxVQUFVO1lBQzNCLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTTtTQUN0QixDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDeEIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQ3hCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQztRQUN4QyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7UUFDdEIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUM1QixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDcEMsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO1FBRWxDLE1BQU0sUUFBUSxHQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUN0RSxJQUFJLENBQUMsWUFBWSxHQUFHLFFBQVEsQ0FBQyxNQUFNLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUM7UUFFOUUsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO0lBQzlCLENBQUM7SUFFRCxNQUFNLENBQUMsZUFBZSxDQUFDLElBQWM7UUFDakMsT0FBTyxJQUFJLFNBQVMsQ0FBQztZQUNqQixFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUNaLFVBQVUsRUFBRSxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxTQUFTO1lBQ3ZDLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxZQUFZO1lBQzFDLEtBQUssRUFBRSxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxVQUFVO1lBQ25DLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxVQUFVO1lBQ3BDLFVBQVUsRUFBRSxJQUFJLENBQUMsU0FBUztZQUMxQixhQUFhLEVBQUUsSUFBSSxDQUFDLGNBQWM7WUFDbEMsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLFVBQVU7WUFDbEMsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJO1lBQ2YsT0FBTyxFQUFFLElBQUksQ0FBQyxRQUFRO1lBQ3RCLFdBQVcsRUFBRSxJQUFJLENBQUMsYUFBYTtZQUMvQixVQUFVLEVBQUUsSUFBSSxDQUFDLFdBQVcsS0FBSyxHQUFHO1lBQ3BDLFlBQVksRUFBRSxJQUFJLENBQUMsYUFBYTtZQUNoQyxNQUFNLEVBQUUsYUFBYSxDQUFDLEtBQUs7U0FDOUIsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELE1BQU0sQ0FBQyxLQUFLO1FBQ1IsT0FBTyxJQUFJLFNBQVMsQ0FBQztZQUNqQixFQUFFLEVBQUUsSUFBSTtZQUNSLFVBQVUsRUFBRSxFQUFFO1lBQ2QsVUFBVSxFQUFFLElBQUk7WUFDaEIsT0FBTyxFQUFFLEVBQUU7WUFDWCxLQUFLLEVBQUUsSUFBSTtZQUNYLEtBQUssRUFBRSxJQUFJO1lBQ1gsYUFBYSxFQUFFLElBQUk7WUFDbkIsSUFBSSxFQUFFLElBQUk7WUFDVixJQUFJLEVBQUUsSUFBSTtZQUNWLE9BQU8sRUFBRSxJQUFJO1lBQ2IsV0FBVyxFQUFFLElBQUk7WUFDakIsVUFBVSxFQUFFLElBQUk7WUFDaEIsWUFBWSxFQUFFLElBQUk7WUFDbEIsTUFBTSxFQUFFLGFBQWEsQ0FBQyxPQUFPO1lBQzdCLFFBQVEsRUFBRSxLQUFLO1NBQ2xCLENBQUMsQ0FBQztJQUNQLENBQUM7Q0FDSiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IElMZWFkQXBpLCBJTGVhZHNBcGksIElMZWFkc1RvdGFsc0FwaSwgSVBlcmlvZHNBcGkgfSBmcm9tICcuL0xlYWRzLWFwaS5wcm9wZXJ0aWVzJztcbmltcG9ydCB7IElNZXNzYWdlQmFzZVByb3BzLCBNZXNzYWdlQmFzZU1vZGVsLCBNZXNzYWdlU291cmNlIH0gZnJvbSAnLi9NZXNzYWdlLm1vZGVsJztcblxuZXhwb3J0IGludGVyZmFjZSBMZWFkc01vZGVsUHJvcHMge1xuICAgIHRvdGFsczogSUxlYWRzVG90YWxzQXBpO1xuICAgIHBlcmlvZHM6IElQZXJpb2RzQXBpO1xuICAgIGxlYWRzOiBMZWFkTW9kZWxbXTtcbiAgICBjaGFydDogbnVtYmVyW107XG59XG5cbmV4cG9ydCBjbGFzcyBMZWFkc01vZGVsIGltcGxlbWVudHMgTGVhZHNNb2RlbFByb3BzIHtcbiAgICBjb25zdHJ1Y3RvcihkYXRhOiBMZWFkc01vZGVsUHJvcHMpIHtcbiAgICAgICAgdGhpcy50b3RhbHMgPSBkYXRhLnRvdGFscztcbiAgICAgICAgdGhpcy5wZXJpb2RzID0gZGF0YS5wZXJpb2RzO1xuICAgICAgICB0aGlzLmxlYWRzID0gZGF0YS5sZWFkcztcbiAgICAgICAgdGhpcy5jaGFydCA9IGRhdGEuY2hhcnQ7XG4gICAgfVxuXG4gICAgdG90YWxzOiBJTGVhZHNUb3RhbHNBcGk7XG4gICAgcGVyaW9kczogSVBlcmlvZHNBcGk7XG4gICAgbGVhZHM6IExlYWRNb2RlbFtdO1xuICAgIGNoYXJ0OiBudW1iZXJbXTtcblxuICAgIHN0YXRpYyBmcm9tQXBpUmVzcG9uc2UoZGF0YTogSUxlYWRzQXBpKSB7XG4gICAgICAgIGNvbnN0IGxlYWRzID0gZGF0YS5tZXNzYWdlcy5tYXAobGVhZCA9PiBMZWFkTW9kZWwuZnJvbUFwaVJlc3BvbnNlKGxlYWQpKTtcblxuICAgICAgICByZXR1cm4gbmV3IExlYWRzTW9kZWwoe1xuICAgICAgICAgICAgdG90YWxzOiBkYXRhLnRvdGFscyxcbiAgICAgICAgICAgIHBlcmlvZHM6IGRhdGEucGVyaW9kcyxcbiAgICAgICAgICAgIGxlYWRzLFxuICAgICAgICAgICAgY2hhcnQ6IGRhdGEuY2hhcnRcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgc3RhdGljIGVtcHR5KCl7XG4gICAgICAgIHJldHVybiBuZXcgTGVhZHNNb2RlbCh7XG4gICAgICAgICAgICB0b3RhbHM6IHtcbiAgICAgICAgICAgICAgICB0b3RhbDogMCxcbiAgICAgICAgICAgICAgICBzZWVuOiAwLFxuICAgICAgICAgICAgICAgIG5ldzogMFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHBlcmlvZHM6IG51bGwsXG4gICAgICAgICAgICBsZWFkczogW10sXG4gICAgICAgICAgICBjaGFydDogW11cbiAgICAgICAgfSk7XG4gICAgfVxufVxuXG5leHBvcnQgaW50ZXJmYWNlIElMZWFkTW9kZWxQcm9wcyBleHRlbmRzIElNZXNzYWdlQmFzZVByb3BzIHtcbiAgICBlbWFpbDogc3RyaW5nO1xuICAgIHBob25lOiBzdHJpbmc7XG4gICAgZm9ybWF0dGVkRGF0ZTogc3RyaW5nO1xuICAgIGRhdGU6IHN0cmluZztcbiAgICBob3VyOiBzdHJpbmc7XG4gICAgdGltZUFnbzogc3RyaW5nO1xuICAgIGxlYWRUaW1lQWdvOiBzdHJpbmc7XG4gICAgcmVhZFN0YXR1czogYm9vbGVhbjtcbiAgICBzaG9ydE1lc3NhZ2U6IHN0cmluZztcbiAgICBmYXZvcml0ZT86IGJvb2xlYW47XG59XG5cbmV4cG9ydCBjbGFzcyBMZWFkTW9kZWwgZXh0ZW5kcyBNZXNzYWdlQmFzZU1vZGVsIGltcGxlbWVudHMgSUxlYWRNb2RlbFByb3BzIHtcbiAgICBlbWFpbDogc3RyaW5nO1xuICAgIHBob25lOiBzdHJpbmc7XG4gICAgZm9ybWF0dGVkRGF0ZTogc3RyaW5nO1xuICAgIGRhdGU6IHN0cmluZztcbiAgICBob3VyOiBzdHJpbmc7XG4gICAgdGltZUFnbzogc3RyaW5nO1xuICAgIGxlYWRUaW1lQWdvOiBzdHJpbmc7XG4gICAgcmVhZFN0YXR1czogYm9vbGVhbjtcbiAgICBzaG9ydE1lc3NhZ2U6IHN0cmluZztcbiAgICBmYXZvcml0ZT86IGJvb2xlYW47XG5cbiAgICBjb25zdHJ1Y3RvcihkYXRhOiBJTGVhZE1vZGVsUHJvcHMpIHtcbiAgICAgICAgc3VwZXIoe1xuICAgICAgICAgICAgaWQ6IGRhdGEuaWQsXG4gICAgICAgICAgICBzZW5kZXJOYW1lOiBkYXRhLnNlbmRlck5hbWUsXG4gICAgICAgICAgICBtZXNzYWdlOiBkYXRhLm1lc3NhZ2UsXG4gICAgICAgICAgICBjcmVhdGVUaW1lOiBkYXRhLmNyZWF0ZVRpbWUsXG4gICAgICAgICAgICBzb3VyY2U6IGRhdGEuc291cmNlXG4gICAgICAgIH0pO1xuXG4gICAgICAgIHRoaXMuZW1haWwgPSBkYXRhLmVtYWlsO1xuICAgICAgICB0aGlzLnBob25lID0gZGF0YS5waG9uZTtcbiAgICAgICAgdGhpcy5mb3JtYXR0ZWREYXRlID0gZGF0YS5mb3JtYXR0ZWREYXRlO1xuICAgICAgICB0aGlzLmRhdGUgPSBkYXRhLmRhdGU7XG4gICAgICAgIHRoaXMuaG91ciA9IGRhdGEuaG91cjtcbiAgICAgICAgdGhpcy50aW1lQWdvID0gZGF0YS50aW1lQWdvO1xuICAgICAgICB0aGlzLmxlYWRUaW1lQWdvID0gZGF0YS5sZWFkVGltZUFnbztcbiAgICAgICAgdGhpcy5yZWFkU3RhdHVzID0gZGF0YS5yZWFkU3RhdHVzO1xuXG4gICAgICAgIGNvbnN0IHNob3J0TXNnID0gKGRhdGEuc2hvcnRNZXNzYWdlIHx8IGRhdGEubWVzc2FnZS5zdWJzdHJpbmcoMCwgMzApKTtcbiAgICAgICAgdGhpcy5zaG9ydE1lc3NhZ2UgPSBzaG9ydE1zZy5sZW5ndGggPj0gMzAgPyBzaG9ydE1zZy5jb25jYXQoJy4uLicpIDogc2hvcnRNc2c7XG5cbiAgICAgICAgdGhpcy5zb3VyY2UgPSBkYXRhLnNvdXJjZTtcbiAgICB9XG5cbiAgICBzdGF0aWMgZnJvbUFwaVJlc3BvbnNlKGRhdGE6IElMZWFkQXBpKTogTGVhZE1vZGVsIHtcbiAgICAgICAgcmV0dXJuIG5ldyBMZWFkTW9kZWwoe1xuICAgICAgICAgICAgaWQ6ICtkYXRhLmlkLFxuICAgICAgICAgICAgc2VuZGVyTmFtZTogZGF0YS5uYW1lIHx8IGRhdGEuZGF0YV9uYW1lLFxuICAgICAgICAgICAgbWVzc2FnZTogZGF0YS5tZXNzYWdlIHx8IGRhdGEuZGF0YV9tZXNzYWdlLFxuICAgICAgICAgICAgZW1haWw6IGRhdGEubWFpbCB8fCBkYXRhLmRhdGFfZW1haWwsXG4gICAgICAgICAgICBwaG9uZTogZGF0YS5waG9uZSB8fCBkYXRhLmRhdGFfcGhvbmUsXG4gICAgICAgICAgICBjcmVhdGVUaW1lOiBkYXRhLnRpbWVzdGFtcCxcbiAgICAgICAgICAgIGZvcm1hdHRlZERhdGU6IGRhdGEuZm9ybWF0dGVkX2RhdGUsXG4gICAgICAgICAgICBkYXRlOiBkYXRhLmRhdGUgfHwgZGF0YS5jcmVhdGVkX2F0LFxuICAgICAgICAgICAgaG91cjogZGF0YS5ob3VyLFxuICAgICAgICAgICAgdGltZUFnbzogZGF0YS50aW1lX2FnbyxcbiAgICAgICAgICAgIGxlYWRUaW1lQWdvOiBkYXRhLmxlYWRfdGltZV9hZ28sXG4gICAgICAgICAgICByZWFkU3RhdHVzOiBkYXRhLnJlYWRfc3RhdHVzID09PSAnMScsXG4gICAgICAgICAgICBzaG9ydE1lc3NhZ2U6IGRhdGEuc2hvcnRfbWVzc2FnZSxcbiAgICAgICAgICAgIHNvdXJjZTogTWVzc2FnZVNvdXJjZS5FTUFJTFxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBzdGF0aWMgZW1wdHkoKTogTGVhZE1vZGVsIHtcbiAgICAgICAgcmV0dXJuIG5ldyBMZWFkTW9kZWwoe1xuICAgICAgICAgICAgaWQ6IG51bGwsXG4gICAgICAgICAgICBzZW5kZXJOYW1lOiAnJyxcbiAgICAgICAgICAgIGNyZWF0ZVRpbWU6IG51bGwsXG4gICAgICAgICAgICBtZXNzYWdlOiAnJyxcbiAgICAgICAgICAgIGVtYWlsOiBudWxsLFxuICAgICAgICAgICAgcGhvbmU6IG51bGwsXG4gICAgICAgICAgICBmb3JtYXR0ZWREYXRlOiBudWxsLFxuICAgICAgICAgICAgZGF0ZTogbnVsbCxcbiAgICAgICAgICAgIGhvdXI6IG51bGwsXG4gICAgICAgICAgICB0aW1lQWdvOiBudWxsLFxuICAgICAgICAgICAgbGVhZFRpbWVBZ286IG51bGwsXG4gICAgICAgICAgICByZWFkU3RhdHVzOiBudWxsLFxuICAgICAgICAgICAgc2hvcnRNZXNzYWdlOiBudWxsLFxuICAgICAgICAgICAgc291cmNlOiBNZXNzYWdlU291cmNlLlVOS05PV04sXG4gICAgICAgICAgICBmYXZvcml0ZTogZmFsc2VcbiAgICAgICAgfSk7XG4gICAgfVxufVxuIl19