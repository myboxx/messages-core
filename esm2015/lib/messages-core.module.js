var MessagesCoreModule_1;
import { __decorate } from "tslib";
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { MessageItemComponent } from './components/message-item/message-item.component';
import { MESSAGES_REPOSITORY } from './repositories/IMessages.repository';
import { MessagesRepository } from './repositories/messages.repository';
import { MESSAGES_SERVICE } from './services/IMessages.service';
import { MessagesService } from './services/messages.service';
import { MessagesEffects } from './state/messages.effects';
import { messagesReducer } from './state/messages.reducer';
import { MessagesStore } from './state/messages.store';
let MessagesCoreModule = MessagesCoreModule_1 = class MessagesCoreModule {
    static forRoot(config) {
        return {
            ngModule: MessagesCoreModule_1,
            providers: [
                { provide: MESSAGES_SERVICE, useClass: MessagesService },
                { provide: MESSAGES_REPOSITORY, useClass: MessagesRepository },
                ...config.providers,
                MessagesStore
            ]
        };
    }
};
MessagesCoreModule = MessagesCoreModule_1 = __decorate([
    NgModule({
        declarations: [MessageItemComponent],
        imports: [
            HttpClientModule,
            StoreModule.forFeature('messages', messagesReducer),
            EffectsModule.forFeature([MessagesEffects]),
            CommonModule,
            FormsModule,
            IonicModule
        ],
        exports: [
            CommonModule,
            FormsModule,
            MessageItemComponent,
        ]
    })
], MessagesCoreModule);
export { MessagesCoreModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVzc2FnZXMtY29yZS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYm94eC9tZXNzYWdlcy1jb3JlLyIsInNvdXJjZXMiOlsibGliL21lc3NhZ2VzLWNvcmUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ3hELE9BQU8sRUFBdUIsUUFBUSxFQUFZLE1BQU0sZUFBZSxDQUFDO0FBQ3hFLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUM3QyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDN0MsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM5QyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sYUFBYSxDQUFDO0FBQzFDLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLGtEQUFrRCxDQUFDO0FBQ3hGLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBQzFFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLG9DQUFvQyxDQUFDO0FBQ3hFLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBQ2hFLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUM5RCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDM0QsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQzNELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQXNCdkQsSUFBYSxrQkFBa0IsMEJBQS9CLE1BQWEsa0JBQWtCO0lBQzNCLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBOEI7UUFDekMsT0FBTztZQUNILFFBQVEsRUFBRSxvQkFBa0I7WUFDNUIsU0FBUyxFQUFFO2dCQUNQLEVBQUUsT0FBTyxFQUFFLGdCQUFnQixFQUFFLFFBQVEsRUFBRSxlQUFlLEVBQUU7Z0JBQ3hELEVBQUUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLFFBQVEsRUFBRSxrQkFBa0IsRUFBQztnQkFDN0QsR0FBRyxNQUFNLENBQUMsU0FBUztnQkFDbkIsYUFBYTthQUNoQjtTQUNKLENBQUM7SUFDTixDQUFDO0NBQ0gsQ0FBQTtBQVpXLGtCQUFrQjtJQWhCOUIsUUFBUSxDQUFDO1FBQ04sWUFBWSxFQUFFLENBQUMsb0JBQW9CLENBQUM7UUFDcEMsT0FBTyxFQUFFO1lBQ0wsZ0JBQWdCO1lBQ2hCLFdBQVcsQ0FBQyxVQUFVLENBQUMsVUFBVSxFQUFFLGVBQWUsQ0FBQztZQUNuRCxhQUFhLENBQUMsVUFBVSxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUM7WUFDM0MsWUFBWTtZQUNaLFdBQVc7WUFDWCxXQUFXO1NBQ2Q7UUFDRCxPQUFPLEVBQUU7WUFDTCxZQUFZO1lBQ1osV0FBVztZQUNYLG9CQUFvQjtTQUN2QjtLQUNKLENBQUM7R0FDVyxrQkFBa0IsQ0FZN0I7U0FaVyxrQkFBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgSHR0cENsaWVudE1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IE1vZHVsZVdpdGhQcm92aWRlcnMsIE5nTW9kdWxlLCBQcm92aWRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRm9ybXNNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgeyBJb25pY01vZHVsZSB9IGZyb20gJ0Bpb25pYy9hbmd1bGFyJztcbmltcG9ydCB7IEVmZmVjdHNNb2R1bGUgfSBmcm9tICdAbmdyeC9lZmZlY3RzJztcbmltcG9ydCB7IFN0b3JlTW9kdWxlIH0gZnJvbSAnQG5ncngvc3RvcmUnO1xuaW1wb3J0IHsgTWVzc2FnZUl0ZW1Db21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvbWVzc2FnZS1pdGVtL21lc3NhZ2UtaXRlbS5jb21wb25lbnQnO1xuaW1wb3J0IHsgTUVTU0FHRVNfUkVQT1NJVE9SWSB9IGZyb20gJy4vcmVwb3NpdG9yaWVzL0lNZXNzYWdlcy5yZXBvc2l0b3J5JztcbmltcG9ydCB7IE1lc3NhZ2VzUmVwb3NpdG9yeSB9IGZyb20gJy4vcmVwb3NpdG9yaWVzL21lc3NhZ2VzLnJlcG9zaXRvcnknO1xuaW1wb3J0IHsgTUVTU0FHRVNfU0VSVklDRSB9IGZyb20gJy4vc2VydmljZXMvSU1lc3NhZ2VzLnNlcnZpY2UnO1xuaW1wb3J0IHsgTWVzc2FnZXNTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9tZXNzYWdlcy5zZXJ2aWNlJztcbmltcG9ydCB7IE1lc3NhZ2VzRWZmZWN0cyB9IGZyb20gJy4vc3RhdGUvbWVzc2FnZXMuZWZmZWN0cyc7XG5pbXBvcnQgeyBtZXNzYWdlc1JlZHVjZXIgfSBmcm9tICcuL3N0YXRlL21lc3NhZ2VzLnJlZHVjZXInO1xuaW1wb3J0IHsgTWVzc2FnZXNTdG9yZSB9IGZyb20gJy4vc3RhdGUvbWVzc2FnZXMuc3RvcmUnO1xuXG5pbnRlcmZhY2UgTW9kdWxlT3B0aW9uc0ludGVyZmFjZSB7XG4gICAgcHJvdmlkZXJzOiBQcm92aWRlcltdO1xufVxuXG5ATmdNb2R1bGUoe1xuICAgIGRlY2xhcmF0aW9uczogW01lc3NhZ2VJdGVtQ29tcG9uZW50XSxcbiAgICBpbXBvcnRzOiBbXG4gICAgICAgIEh0dHBDbGllbnRNb2R1bGUsXG4gICAgICAgIFN0b3JlTW9kdWxlLmZvckZlYXR1cmUoJ21lc3NhZ2VzJywgbWVzc2FnZXNSZWR1Y2VyKSxcbiAgICAgICAgRWZmZWN0c01vZHVsZS5mb3JGZWF0dXJlKFtNZXNzYWdlc0VmZmVjdHNdKSxcbiAgICAgICAgQ29tbW9uTW9kdWxlLFxuICAgICAgICBGb3Jtc01vZHVsZSxcbiAgICAgICAgSW9uaWNNb2R1bGVcbiAgICBdLFxuICAgIGV4cG9ydHM6IFtcbiAgICAgICAgQ29tbW9uTW9kdWxlLFxuICAgICAgICBGb3Jtc01vZHVsZSxcbiAgICAgICAgTWVzc2FnZUl0ZW1Db21wb25lbnQsXG4gICAgXVxufSlcbmV4cG9ydCBjbGFzcyBNZXNzYWdlc0NvcmVNb2R1bGUge1xuICAgIHN0YXRpYyBmb3JSb290KGNvbmZpZzogTW9kdWxlT3B0aW9uc0ludGVyZmFjZSk6IE1vZHVsZVdpdGhQcm92aWRlcnM8TWVzc2FnZXNDb3JlTW9kdWxlPiB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBuZ01vZHVsZTogTWVzc2FnZXNDb3JlTW9kdWxlLFxuICAgICAgICAgICAgcHJvdmlkZXJzOiBbXG4gICAgICAgICAgICAgICAgeyBwcm92aWRlOiBNRVNTQUdFU19TRVJWSUNFLCB1c2VDbGFzczogTWVzc2FnZXNTZXJ2aWNlIH0sXG4gICAgICAgICAgICAgICAgeyBwcm92aWRlOiBNRVNTQUdFU19SRVBPU0lUT1JZLCB1c2VDbGFzczogTWVzc2FnZXNSZXBvc2l0b3J5fSxcbiAgICAgICAgICAgICAgICAuLi5jb25maWcucHJvdmlkZXJzLFxuICAgICAgICAgICAgICAgIE1lc3NhZ2VzU3RvcmVcbiAgICAgICAgICAgIF1cbiAgICAgICAgfTtcbiAgICB9XG4gfVxuIl19