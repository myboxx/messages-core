import { __decorate, __param } from 'tslib';
import { CommonModule } from '@angular/common';
import { HttpParams, HttpClient, HttpClientModule } from '@angular/common/http';
import { Input, Component, InjectionToken, Inject, Injectable, ɵɵdefineInjectable, ɵɵinject, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { createEffect, ofType, Actions, EffectsModule } from '@ngrx/effects';
import { createAction, props, Store, createReducer, on, createFeatureSelector, createSelector, StoreModule } from '@ngrx/store';
import { AbstractAppConfigService, APP_CONFIG_SERVICE } from '@boxx/core';
import { forkJoin, of } from 'rxjs';
import { catchError, map, switchMap, withLatestFrom } from 'rxjs/operators';

let MessageItemComponent = class MessageItemComponent {
    constructor() {
        this.detail = true;
    }
    ngOnInit() {
    }
};
__decorate([
    Input()
], MessageItemComponent.prototype, "detail", void 0);
MessageItemComponent = __decorate([
    Component({
        selector: 'boxx-message-item',
        template: "<div class=\"message-item-row\">\n    <div class=\"message-item-row-content\">\n        <div class=\"message-item-row-content-top\">\n            <div class=\"top-icon-container\">\n                <ng-content select=\"div[slot=top-left]\"></ng-content>\n            </div>\n            <ion-row>\n                <ion-col class=\"col-left\">\n                    <div>\n                        <ng-content select=\"div[slot=top-center]\"></ng-content>\n                    </div>\n                </ion-col>\n                <ion-col class=\"col-right\">\n                    <div>\n                        <ng-content select=\"div[slot=top-right]\"></ng-content>\n                    </div>\n                </ion-col>\n            </ion-row>\n        </div>\n\n        <ion-row class=\"bottom-row\">\n            <ion-col class=\"left-col\" size=\"6\">\n                <div class=\"bottom-left\">\n                    <ng-content select=\"ion-label[slot=bottom-start]\"></ng-content>\n                </div>\n            </ion-col>\n            <ion-col class=\"right-col\" size=\"6\">\n                <div class=\"right\">\n                    <div class=\"top\">\n                        <ng-content select=\"div[slot=bottom-end-top]\"></ng-content>\n                    </div>\n                    <div class=\"bottom\">\n                        <ng-content select=\"div[slot=bottom-end-bottom]\"></ng-content>\n                    </div>\n                </div>\n            </ion-col>\n        </ion-row>\n        <ng-content></ng-content>\n    </div>\n\n    <div class=\"message-detail-icon\">\n        <ion-icon name=\"chevron-forward-outline\" [hidden]=\"!detail\"></ion-icon>\n    </div>\n</div>",
        styles: [":host .message-item-row{display:flex;justify-content:space-between;align-items:center;padding:16px 0 16px 16px}:host .message-item-row ion-col{padding:0}:host .message-item-row-content{width:100%}:host .message-item-row-content-top{display:flex;justify-content:space-between;margin-bottom:8px}:host .message-item-row-content-top .top-icon-container{font-size:x-large;width:36px;height:36px;background:#d3d3d3;border-radius:8px}:host .message-item-row-content-top ion-row{width:100%}:host .message-item-row-content-top .col-left{white-space:normal;padding-left:8px}:host .message-item-row-content-top .col-right{padding-top:3px;font-size:smaller;text-align:right}:host .bottom-row .left-col{margin-top:8px}:host .bottom-row .right-col .right{display:flex;height:100%;flex-wrap:wrap;align-content:space-between;justify-content:space-evenly}:host .bottom-row .right-col .right .top{width:100%;display:flex;justify-content:flex-end}:host .bottom-row .right-col .right .bottom{width:100%;text-align:right}:host .message-detail-icon{min-width:32px}:host .message-detail-icon ion-icon{margin-left:8px;font-size:x-large}"]
    })
], MessageItemComponent);

const MESSAGES_REPOSITORY = new InjectionToken('messagesRepository');

let MessagesRepository = class MessagesRepository {
    constructor(appSettings, httpClient) {
        this.appSettings = appSettings;
        this.httpClient = httpClient;
    }
    getLeads() {
        return this.httpClient.get(`${this.getBaseUrl()}/leads`);
    }
    setLeadAsRead(id) {
        const data = { lead_id: id };
        const urlSearchParams = new URLSearchParams();
        Object.keys(data).forEach((key, i) => {
            urlSearchParams.append(key, data[key]);
        });
        const body = urlSearchParams.toString();
        return this.httpClient.post(`${this.getBaseUrl()}/leads/mark_message_as_read`, body);
    }
    getReviews() {
        return this.httpClient.get(`${this.getBaseUrl()}/gmb/reviews`);
    }
    getReview(reviewId) {
        return this.httpClient.get(`${this.getBaseUrl()}/gmb/review/${reviewId}`);
    }
    upsertReviewReply(reviewId, comment) {
        let params = new HttpParams();
        params = params.append('comment', comment);
        return this.httpClient.post(`${this.getBaseUrl()}/gmb/upsertReview/${reviewId}`, params.toString());
    }
    deleteReviewReply(reviewId) {
        return this.httpClient.delete(`${this.getBaseUrl()}/gmb/deleteReview/${reviewId}`);
    }
    getBaseUrl() {
        return `${this.appSettings.baseUrl()}/api/${this.appSettings.instance()}/v1`;
    }
};
MessagesRepository.ctorParameters = () => [
    { type: AbstractAppConfigService, decorators: [{ type: Inject, args: [APP_CONFIG_SERVICE,] }] },
    { type: HttpClient }
];
MessagesRepository = __decorate([
    Injectable(),
    __param(0, Inject(APP_CONFIG_SERVICE))
], MessagesRepository);

const MESSAGES_SERVICE = new InjectionToken('messagesService');

/**
 * Superclass for any type of message:
 * Lead and Review by now
 */
class MessageBaseModel {
    constructor(data) {
        this.id = data.id;
        this.senderName = data.senderName;
        this.message = data.message;
        this.createTime = data.createTime;
        this.source = data.source;
    }
}
var MessageSource;
(function (MessageSource) {
    MessageSource[MessageSource["EMAIL"] = 0] = "EMAIL";
    MessageSource[MessageSource["GMB_MESSAGES"] = 1] = "GMB_MESSAGES";
    MessageSource[MessageSource["GMB_REVIEW"] = 2] = "GMB_REVIEW";
    MessageSource[MessageSource["CHAT_WEBSITE"] = 3] = "CHAT_WEBSITE";
    MessageSource[MessageSource["UNKNOWN"] = 4] = "UNKNOWN";
})(MessageSource || (MessageSource = {}));

class LeadsModel {
    constructor(data) {
        this.totals = data.totals;
        this.periods = data.periods;
        this.leads = data.leads;
        this.chart = data.chart;
    }
    static fromApiResponse(data) {
        const leads = data.messages.map(lead => LeadModel.fromApiResponse(lead));
        return new LeadsModel({
            totals: data.totals,
            periods: data.periods,
            leads,
            chart: data.chart
        });
    }
    static empty() {
        return new LeadsModel({
            totals: {
                total: 0,
                seen: 0,
                new: 0
            },
            periods: null,
            leads: [],
            chart: []
        });
    }
}
class LeadModel extends MessageBaseModel {
    constructor(data) {
        super({
            id: data.id,
            senderName: data.senderName,
            message: data.message,
            createTime: data.createTime,
            source: data.source
        });
        this.email = data.email;
        this.phone = data.phone;
        this.formattedDate = data.formattedDate;
        this.date = data.date;
        this.hour = data.hour;
        this.timeAgo = data.timeAgo;
        this.leadTimeAgo = data.leadTimeAgo;
        this.readStatus = data.readStatus;
        const shortMsg = (data.shortMessage || data.message.substring(0, 30));
        this.shortMessage = shortMsg.length >= 30 ? shortMsg.concat('...') : shortMsg;
        this.source = data.source;
    }
    static fromApiResponse(data) {
        return new LeadModel({
            id: +data.id,
            senderName: data.name || data.data_name,
            message: data.message || data.data_message,
            email: data.mail || data.data_email,
            phone: data.phone || data.data_phone,
            createTime: data.timestamp,
            formattedDate: data.formatted_date,
            date: data.date || data.created_at,
            hour: data.hour,
            timeAgo: data.time_ago,
            leadTimeAgo: data.lead_time_ago,
            readStatus: data.read_status === '1',
            shortMessage: data.short_message,
            source: MessageSource.EMAIL
        });
    }
    static empty() {
        return new LeadModel({
            id: null,
            senderName: '',
            createTime: null,
            message: '',
            email: null,
            phone: null,
            formattedDate: null,
            date: null,
            hour: null,
            timeAgo: null,
            leadTimeAgo: null,
            readStatus: null,
            shortMessage: null,
            source: MessageSource.UNKNOWN,
            favorite: false
        });
    }
}

class MessagesPageModel {
    constructor(data) {
        this.stats = data.stats;
        this.messages = data.messages;
    }
    static empty() {
        return new MessagesPageModel({
            stats: {
                leads: {
                    total: 0,
                    read: 0,
                    new: 0
                },
                reviews: {
                    averageRating: 0,
                    totalReviewCount: 0
                }
            },
            messages: [],
        });
    }
}

class ReviewsModel {
    constructor(data) {
        this.averageRating = data.averageRating;
        this.reviews = data.reviews;
        this.totalReviewCount = data.totalReviewCount;
    }
    static fromApiResponse(data) {
        const reviewList = (!Array.isArray(data.reviews) ? [] : data.reviews).map(review => ReviewModel.fromApiResponse({
            id: review.id,
            comment: review.comment,
            createTime: review.createTime,
            reviewer: review.reviewer,
            starRating: review.starRating,
            updateTime: review.updateTime,
            reviewReply: review.reviewReply
        }));
        return new ReviewsModel({
            averageRating: data.averageRating || 0,
            reviews: reviewList,
            totalReviewCount: data.totalReviewCount || 0
        });
    }
    empty() {
        return new ReviewsModel({
            averageRating: null,
            reviews: null,
            totalReviewCount: null
        });
    }
}
class ReviewModel extends MessageBaseModel {
    constructor(data) {
        super({
            id: data.id,
            senderName: data.senderName,
            message: data.message,
            createTime: data.createTime,
            source: data.source
        });
        this.senderPhotoUrl = data.senderPhotoUrl;
        this.starRating = data.starRating;
        this.updateTime = data.updateTime;
        this.reviewReply = data.reviewReply;
    }
    static fromApiResponse(data) {
        return new ReviewModel({
            // base class properties:
            id: data.id,
            senderName: data.reviewer.displayName,
            message: data.comment,
            createTime: data.createTime,
            source: MessageSource.GMB_REVIEW,
            // own (extended) properties:
            senderPhotoUrl: data.reviewer.profilePhotoUrl,
            starRating: data.starRating,
            updateTime: data.updateTime,
            reviewReply: data.reviewReply
        });
    }
}

let MessagesService = class MessagesService {
    constructor(repository) {
        this.repository = repository;
    }
    getMessages(sorting) {
        let leadsError;
        let ReviewsError;
        return forkJoin([
            this.getLeads().pipe(catchError(err => { leadsError = err; return of(null); })),
            this.getReviews().pipe(catchError(err => { ReviewsError = err; return of(null); })),
        ]).pipe(map(([leadsData, reviewsData]) => {
            return new MessagesPageModel({
                stats: {
                    leads: {
                        error: leadsError,
                        total: leadsError ? 0 : leadsData.totals.total,
                        read: leadsError ? 0 : leadsData.totals.seen,
                        new: leadsError ? 0 : leadsData.totals.new
                    },
                    reviews: {
                        error: ReviewsError,
                        averageRating: ReviewsError ? 0 : reviewsData.averageRating,
                        totalReviewCount: ReviewsError ? 0 : reviewsData.totalReviewCount
                    }
                },
                messages: [
                    ...(leadsError ? [] : [...leadsData.leads]),
                    ...(ReviewsError ? [] : [...reviewsData.reviews])
                ].sort((a, b) => {
                    return (sorting === 'ASC' ?
                        b.createTime.localeCompare(a.createTime) :
                        a.createTime.localeCompare(b.createTime));
                }),
            });
        }));
    }
    getLeads() {
        return this.repository.getLeads().pipe(map(response => {
            return LeadsModel.fromApiResponse(response.data);
        }));
    }
    setLeadAsRead(id) {
        return this.repository.setLeadAsRead(id).pipe(map(response => {
            return LeadModel.fromApiResponse(response.data);
        }));
    }
    getReviews() {
        return this.repository.getReviews().pipe(map(response => {
            return ReviewsModel.fromApiResponse(response.data);
        }));
    }
    getReview(reviewId) {
        return this.repository.getReview(reviewId).pipe(map(response => {
            return ReviewModel.fromApiResponse(response.data);
        }));
    }
    upsertReviewReply(review, comment) {
        return this.repository.upsertReviewReply(review.id, comment).pipe(map(response => {
            return new ReviewModel({
                id: review.id,
                senderName: review.senderName,
                message: review.message,
                createTime: review.createTime,
                source: review.source,
                senderPhotoUrl: review.senderPhotoUrl,
                starRating: review.starRating,
                updateTime: review.updateTime,
                reviewReply: {
                    comment,
                    updateTime: response.data.updateTime,
                }
            });
        }));
    }
    deleteReviewReply(review) {
        return this.repository.deleteReviewReply(review.id).pipe(map(response => {
            if (response.status === 'success') {
                return new ReviewModel({
                    id: review.id,
                    senderName: review.senderName,
                    message: review.message,
                    createTime: review.createTime,
                    source: review.source,
                    senderPhotoUrl: review.senderPhotoUrl,
                    starRating: review.starRating,
                    updateTime: review.updateTime
                });
            }
            else {
                throw Error(response.message || 'Unknown error');
            }
        }));
    }
};
MessagesService.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [MESSAGES_REPOSITORY,] }] }
];
MessagesService.ɵprov = ɵɵdefineInjectable({ factory: function MessagesService_Factory() { return new MessagesService(ɵɵinject(MESSAGES_REPOSITORY)); }, token: MessagesService, providedIn: "root" });
MessagesService = __decorate([
    Injectable({
        providedIn: 'root'
    }),
    __param(0, Inject(MESSAGES_REPOSITORY))
], MessagesService);

var MessagesActionTypes;
(function (MessagesActionTypes) {
    MessagesActionTypes["GetMessagesBegin"] = "[Messages] Get Messages begin";
    MessagesActionTypes["GetMessagesSuccess"] = "[Messages] Get Messages success";
    MessagesActionTypes["GetMessagesFail"] = "[Messages] Get Messages failure";
    MessagesActionTypes["SetLeadAsReadBegin"] = "[Messages] Set Lead as Read begin";
    MessagesActionTypes["SetLeadAsReadSuccess"] = "[Messages] Set Messages as Read success";
    MessagesActionTypes["SetLeadAsReadFail"] = "[Messages] Set Messages as Read failure";
    MessagesActionTypes["UpsertReviewReplyBegin"] = "[Messages] Upsert Review Reply begin";
    MessagesActionTypes["UpsertReviewReplySuccess"] = "[Messages] Upsert Review Reply success";
    MessagesActionTypes["UpsertReviewReplyFail"] = "[Messages] Upsert Review Reply failure";
    MessagesActionTypes["DeleteReviewReplyBegin"] = "[Messages] Delete Review Reply begin";
    MessagesActionTypes["DeleteReviewReplySuccess"] = "[Messages] Delete Review Reply success";
    MessagesActionTypes["DeleteReviewReplyFail"] = "[Messages] Delete Review Reply failure";
    MessagesActionTypes["FilterMessagesByReadStatusBegin"] = "[Messages] Filter message by Read Status begin";
    MessagesActionTypes["FilterMessagesByReadStatusSuccess"] = "[Messages] Filter message success";
    MessagesActionTypes["FilterMessagesBySourceBegin"] = "[Messages] Filter message by Source begin";
    MessagesActionTypes["FilterMessagesSourceSuccess"] = "[Messages] Filter message by Source success";
    MessagesActionTypes["SortMessagesByDateBegin"] = "[Messages] Sort message by Date begin";
    MessagesActionTypes["SortMessagesByDateSuccess"] = "[Messages] Sort message by Date success";
    MessagesActionTypes["SelectMessage"] = "[Messages] Select message";
})(MessagesActionTypes || (MessagesActionTypes = {}));
// GET Messages from remote API
const GetMessagesBeginAction = createAction(MessagesActionTypes.GetMessagesBegin, props());
const GetMessagesSuccessAction = createAction(MessagesActionTypes.GetMessagesSuccess, props());
const GetMessagesFailAction = createAction(MessagesActionTypes.GetMessagesFail, props());
// SET AS READ
const SetLeadAsReadBeginAction = createAction(MessagesActionTypes.SetLeadAsReadBegin, props());
const SetLeadAsReadSuccessAction = createAction(MessagesActionTypes.SetLeadAsReadSuccess, props());
const SetLeadAsReadFailAction = createAction(MessagesActionTypes.SetLeadAsReadFail, props());
const UpsertReviewReplyBeginAction = createAction(MessagesActionTypes.UpsertReviewReplyBegin, props());
const UpsertReviewReplySuccessAction = createAction(MessagesActionTypes.UpsertReviewReplySuccess, props());
const UpsertReviewReplyFaliAction = createAction(MessagesActionTypes.UpsertReviewReplyFail, props());
const DeleteReviewReplyBeginAction = createAction(MessagesActionTypes.DeleteReviewReplyBegin, props());
const DeleteReviewReplySuccessAction = createAction(MessagesActionTypes.DeleteReviewReplySuccess, props());
const DeleteReviewReplyFaliAction = createAction(MessagesActionTypes.DeleteReviewReplyFail, props());
// FILTERING
const FilterMessagesByReadStatusBeginAction = createAction(MessagesActionTypes.FilterMessagesByReadStatusBegin, props());
const FilterMessagesByReadStatusSuccessAction = createAction(MessagesActionTypes.FilterMessagesByReadStatusSuccess, props());
const FilterMessagesBySourceBeginAction = createAction(MessagesActionTypes.FilterMessagesBySourceBegin, props());
const FilterMessagesBySourceSuccessAction = createAction(MessagesActionTypes.FilterMessagesSourceSuccess, props());
const SelectMessageAction = createAction(MessagesActionTypes.SelectMessage, props());

let MessagesEffects = class MessagesEffects {
    constructor(actions$, store$, service) {
        this.actions$ = actions$;
        this.store$ = store$;
        this.service = service;
        this.load$ = createEffect(() => this.actions$.pipe(ofType(MessagesActionTypes.GetMessagesBegin), switchMap((action) => {
            return this.service.getMessages(action.sorting).pipe(map((data) => GetMessagesSuccessAction({ data })), catchError(error => {
                console.error('Couldn\'t get messages', error);
                return of(GetMessagesFailAction({ errors: error }));
            }));
        })));
        this.setAsRead$ = createEffect(() => this.actions$.pipe(ofType(MessagesActionTypes.SetLeadAsReadBegin), switchMap((action) => {
            return this.service.setLeadAsRead(action.id).pipe(map((message) => SetLeadAsReadSuccessAction({ message })), catchError(error => {
                console.error('Couldn\'t set message as read', error);
                return of(SetLeadAsReadFailAction({ errors: error }));
            }));
        })));
        this.filter$ = createEffect(() => this.actions$.pipe(ofType(MessagesActionTypes.FilterMessagesByReadStatusBegin), withLatestFrom(this.store$), switchMap(([action, store]) => {
            const messageType = action.messageType;
            let messageList = store.messages.pageData.messages.filter(item => store.messages.filteredSources.includes(item.source));
            messageList = messageType === 'ALL' ? messageList :
                messageList.filter(item => {
                    if (messageType === 'READ' && item.readStatus === undefined) {
                        return true;
                    }
                    return item.readStatus === (messageType === 'READ' ? true : false);
                });
            return of(FilterMessagesByReadStatusSuccessAction({ messageList }));
        })));
        this.sortAndFilter$ = createEffect(() => this.actions$.pipe(ofType(MessagesActionTypes.FilterMessagesBySourceBegin), withLatestFrom(this.store$), switchMap(([action, store]) => {
            const sources = action.sources;
            const sorting = action.sorting;
            const sortedMessages = store.messages.pageData.messages
                .map(m => m) // <-- because pageData.messages is READ ONLY!!
                .sort((a, b) => {
                return (sorting === 'ASC' ?
                    b.createTime.localeCompare(a.createTime) :
                    a.createTime.localeCompare(b.createTime));
            });
            const filteredMessages = sortedMessages.filter(item => sources.includes(item.source));
            return of(FilterMessagesBySourceSuccessAction({
                sorting,
                sources,
                filteredMessages
            }));
        })));
        this.upsertReviewReply$ = createEffect(() => this.actions$.pipe(ofType(UpsertReviewReplyBeginAction), switchMap((action) => {
            return this.service.upsertReviewReply(action.review, action.comment).pipe(map(review => UpsertReviewReplySuccessAction({ review })), catchError(error => {
                console.error('Couldn\'t upsert review reply', error);
                return of(UpsertReviewReplyFaliAction({ errors: error }));
            }));
        })));
        this.deleteReviewReply$ = createEffect(() => this.actions$.pipe(ofType(DeleteReviewReplyBeginAction), switchMap((action) => {
            return this.service.deleteReviewReply(action.review).pipe(map(review => DeleteReviewReplySuccessAction({ review })), catchError(error => {
                console.error('Couldn\'t delete review reply', error);
                return of(DeleteReviewReplyFaliAction({ errors: error }));
            }));
        })));
    }
};
MessagesEffects.ctorParameters = () => [
    { type: Actions },
    { type: Store },
    { type: undefined, decorators: [{ type: Inject, args: [MESSAGES_SERVICE,] }] }
];
MessagesEffects = __decorate([
    Injectable(),
    __param(2, Inject(MESSAGES_SERVICE))
], MessagesEffects);

const initialState = {
    isLoading: false,
    hasBeenFetched: false,
    pageData: MessagesPageModel.empty(),
    filteredSources: [],
    filteredItems: [],
    sorting: 'ASC',
    selectedId: null,
    error: null,
    success: null
};
const ɵ0 = (state) => (Object.assign(Object.assign({}, state), { error: null, success: null, isLoading: true })), ɵ1 = (state, action) => (Object.assign(Object.assign({}, state), { isLoading: false, hasBeenFetched: true, pageData: Object.assign(Object.assign({}, state.pageData), { stats: action.data.stats, messages: action.data.messages }), error: null, success: { after: getAfterActionType(action.type) } })), ɵ2 = (state, action) => (Object.assign(Object.assign({}, state), { isLoading: false, hasBeenFetched: true, pageData: Object.assign(Object.assign({}, state.pageData), { messages: state.pageData.messages.map(m => {
            return m.id === action.review.id ? action.review : m;
        }) }), error: null, success: { after: getAfterActionType(action.type) } })), ɵ3 = (state, action) => (Object.assign(Object.assign({}, state), { pageData: Object.assign(Object.assign({}, state.pageData), { stats: Object.assign(Object.assign({}, state.pageData.stats), { leads: Object.assign(Object.assign({}, state.pageData.stats.leads), { read: state.pageData.stats.leads.read + 1, new: state.pageData.stats.leads.new - 1 }) }), messages: [
            ...((ml) => {
                const tmp = [...ml];
                const idx = ml.findIndex((m) => m.id === action.message.id);
                if (idx !== -1) {
                    tmp.splice(idx, 1, action.message);
                }
                return tmp;
            })(state.pageData.messages),
        ] }), error: null, success: { after: getAfterActionType(action.type) } })), ɵ4 = (state, action) => (Object.assign(Object.assign({}, state), { isLoading: false, error: { after: getAfterActionType(action.type), error: action.errors } })), ɵ5 = (state, action) => (Object.assign(Object.assign({}, state), { error: { after: getAfterActionType(action.type), error: action.errors } })), ɵ6 = (state, action) => (Object.assign(Object.assign({}, state), { filteredItems: action.messageList, success: null })), ɵ7 = (state, action) => (Object.assign(Object.assign({}, state), { sorting: action.sorting, filteredSources: action.sources, filteredItems: action.filteredMessages, success: null })), ɵ8 = (state, action) => (Object.assign(Object.assign({}, state), { selectedId: action.messageId, success: null }));
const reducer = createReducer(initialState, 
// On Begin Actions
on(GetMessagesBeginAction, UpsertReviewReplyBeginAction, DeleteReviewReplyBeginAction, ɵ0), 
// ON Success Actions
on(GetMessagesSuccessAction, ɵ1), on(UpsertReviewReplySuccessAction, DeleteReviewReplySuccessAction, ɵ2), on(SetLeadAsReadSuccessAction, ɵ3), 
// ON Fail Actions
on(GetMessagesFailAction, UpsertReviewReplyFaliAction, DeleteReviewReplyFaliAction, ɵ4), on(SetLeadAsReadFailAction, ɵ5), 
// FILTER
on(FilterMessagesByReadStatusSuccessAction, ɵ6), on(FilterMessagesBySourceSuccessAction, ɵ7), 
// SELECT
on(SelectMessageAction, ɵ8));
function getAfterActionType(type) {
    let action;
    switch (type) {
        case MessagesActionTypes.GetMessagesSuccess:
        case MessagesActionTypes.GetMessagesFail:
            action = 'GET';
            break;
        case MessagesActionTypes.SetLeadAsReadSuccess:
        case MessagesActionTypes.SetLeadAsReadFail:
            action = 'SET_LEAD_READ';
            break;
        case MessagesActionTypes.UpsertReviewReplySuccess:
        case MessagesActionTypes.UpsertReviewReplyFail:
            action = 'UPSERT_REVIEW';
            break;
        case MessagesActionTypes.DeleteReviewReplyFail:
        case MessagesActionTypes.DeleteReviewReplySuccess:
            action = 'DELETE_REVIEW';
            break;
        default:
            action = 'UNKNOWN';
    }
    return action;
}
function messagesReducer(state, action) {
    return reducer(state, action);
}

const getMessagesState = createFeatureSelector('messages');
const stateGetPageData = (state) => state.pageData;
const stateGetIsLoading = (state) => state.isLoading;
const stateGetMessages = (state) => state.pageData.messages;
const stateGetLeads = (state) => state.pageData.messages.filter(m => m.source === MessageSource.EMAIL);
const stateGetReviews = (state) => state.pageData.messages.filter(m => m.source === MessageSource.GMB_REVIEW);
const stateGetFilteredItems = (state) => state.filteredItems;
const ɵ0$1 = state => state;
const getMessagesPageState = createSelector(getMessagesState, ɵ0$1);
const getMessagesPageData = createSelector(getMessagesState, stateGetPageData);
const getMessages = createSelector(getMessagesPageState, stateGetMessages);
const getLeads = createSelector(getMessagesPageState, stateGetLeads);
const getReviews = createSelector(getMessagesPageState, stateGetReviews);
const getIsLoading = createSelector(getMessagesPageState, stateGetIsLoading);
const ɵ1$1 = state => state.error;
const getError = createSelector(getMessagesPageState, ɵ1$1);
const ɵ2$1 = state => state.success;
const getSuccess = createSelector(getMessagesPageState, ɵ2$1);
const getFilteredMessages = createSelector(getMessagesPageState, stateGetFilteredItems);
const ɵ3$1 = state => state.pageData.messages.filter(m => m.id === state.selectedId)[0];
const getMessageById = createSelector(getMessagesPageState, ɵ3$1);
const ɵ4$1 = state => state.hasBeenFetched;
const hasBeenFetched = createSelector(getMessagesPageState, ɵ4$1);

let MessagesStore = class MessagesStore {
    constructor(store) {
        this.store = store;
    }
    get Loading$() { return this.store.select(getIsLoading); }
    get Error$() {
        return this.store.select(getError);
    }
    get Success$() {
        return this.store.select(getSuccess);
    }
    loadMessagesPage(sorting) {
        return this.store.dispatch(GetMessagesBeginAction({ sorting }));
    }
    get MessagesPageData$() {
        return this.store.select(getMessagesPageData);
    }
    filterMessagesByReadStatus(criteria) {
        this.store.dispatch(FilterMessagesByReadStatusBeginAction({ messageType: criteria }));
    }
    filterMessagesBySource(sorting = 'ASC', sources) {
        this.store.dispatch(FilterMessagesBySourceBeginAction({ sorting, sources }));
    }
    get FilteredMessages$() {
        return this.store.select(getFilteredMessages);
    }
    MessageById$(messageId) {
        this.store.dispatch(SelectMessageAction({ messageId }));
        return this.store.select(getMessageById);
    }
    get Leads$() {
        return this.store.select(getLeads);
    }
    setLeadAsRead(id) {
        this.store.dispatch(SetLeadAsReadBeginAction({ id }));
    }
    get Reviews$() {
        return this.store.select(getReviews);
    }
    upsertReviewReply(review, comment) {
        this.store.dispatch(UpsertReviewReplyBeginAction({ review, comment }));
    }
    deleteReviewReply(review) {
        this.store.dispatch(DeleteReviewReplyBeginAction({ review }));
    }
    get HasBeenFetched$() {
        return this.store.select(hasBeenFetched);
    }
};
MessagesStore.ctorParameters = () => [
    { type: Store }
];
MessagesStore = __decorate([
    Injectable()
], MessagesStore);

var MessagesCoreModule_1;
let MessagesCoreModule = MessagesCoreModule_1 = class MessagesCoreModule {
    static forRoot(config) {
        return {
            ngModule: MessagesCoreModule_1,
            providers: [
                { provide: MESSAGES_SERVICE, useClass: MessagesService },
                { provide: MESSAGES_REPOSITORY, useClass: MessagesRepository },
                ...config.providers,
                MessagesStore
            ]
        };
    }
};
MessagesCoreModule = MessagesCoreModule_1 = __decorate([
    NgModule({
        declarations: [MessageItemComponent],
        imports: [
            HttpClientModule,
            StoreModule.forFeature('messages', messagesReducer),
            EffectsModule.forFeature([MessagesEffects]),
            CommonModule,
            FormsModule,
            IonicModule
        ],
        exports: [
            CommonModule,
            FormsModule,
            MessageItemComponent,
        ]
    })
], MessagesCoreModule);

/*
 * Public API Surface of messages
 */

/**
 * Generated bundle index. Do not edit.
 */

export { DeleteReviewReplyBeginAction, DeleteReviewReplyFaliAction, DeleteReviewReplySuccessAction, FilterMessagesByReadStatusBeginAction, FilterMessagesByReadStatusSuccessAction, FilterMessagesBySourceBeginAction, FilterMessagesBySourceSuccessAction, GetMessagesBeginAction, GetMessagesFailAction, GetMessagesSuccessAction, LeadModel, LeadsModel, MESSAGES_REPOSITORY, MESSAGES_SERVICE, MessageBaseModel, MessageSource, MessagesActionTypes, MessagesCoreModule, MessagesEffects, MessagesPageModel, MessagesRepository, MessagesService, MessagesStore, ReviewModel, ReviewsModel, SelectMessageAction, SetLeadAsReadBeginAction, SetLeadAsReadFailAction, SetLeadAsReadSuccessAction, UpsertReviewReplyBeginAction, UpsertReviewReplyFaliAction, UpsertReviewReplySuccessAction, getError, getFilteredMessages, getIsLoading, getLeads, getMessageById, getMessages, getMessagesPageData, getMessagesPageState, getMessagesState, getReviews, getSuccess, hasBeenFetched, initialState, messagesReducer, stateGetFilteredItems, stateGetIsLoading, stateGetLeads, stateGetMessages, stateGetPageData, stateGetReviews, ɵ0$1 as ɵ0, ɵ1$1 as ɵ1, ɵ2$1 as ɵ2, ɵ3$1 as ɵ3, ɵ4$1 as ɵ4, MessageItemComponent as ɵa };
//# sourceMappingURL=boxx-messages-core.js.map
