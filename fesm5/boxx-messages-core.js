import { __decorate, __param, __extends, __read, __spread, __assign } from 'tslib';
import { CommonModule } from '@angular/common';
import { HttpParams, HttpClient, HttpClientModule } from '@angular/common/http';
import { Input, Component, InjectionToken, Inject, Injectable, ɵɵdefineInjectable, ɵɵinject, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { createEffect, ofType, Actions, EffectsModule } from '@ngrx/effects';
import { createAction, props, Store, createReducer, on, createFeatureSelector, createSelector, StoreModule } from '@ngrx/store';
import { AbstractAppConfigService, APP_CONFIG_SERVICE } from '@boxx/core';
import { forkJoin, of } from 'rxjs';
import { catchError, map, switchMap, withLatestFrom } from 'rxjs/operators';

var MessageItemComponent = /** @class */ (function () {
    function MessageItemComponent() {
        this.detail = true;
    }
    MessageItemComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Input()
    ], MessageItemComponent.prototype, "detail", void 0);
    MessageItemComponent = __decorate([
        Component({
            selector: 'boxx-message-item',
            template: "<div class=\"message-item-row\">\n    <div class=\"message-item-row-content\">\n        <div class=\"message-item-row-content-top\">\n            <div class=\"top-icon-container\">\n                <ng-content select=\"div[slot=top-left]\"></ng-content>\n            </div>\n            <ion-row>\n                <ion-col class=\"col-left\">\n                    <div>\n                        <ng-content select=\"div[slot=top-center]\"></ng-content>\n                    </div>\n                </ion-col>\n                <ion-col class=\"col-right\">\n                    <div>\n                        <ng-content select=\"div[slot=top-right]\"></ng-content>\n                    </div>\n                </ion-col>\n            </ion-row>\n        </div>\n\n        <ion-row class=\"bottom-row\">\n            <ion-col class=\"left-col\" size=\"6\">\n                <div class=\"bottom-left\">\n                    <ng-content select=\"ion-label[slot=bottom-start]\"></ng-content>\n                </div>\n            </ion-col>\n            <ion-col class=\"right-col\" size=\"6\">\n                <div class=\"right\">\n                    <div class=\"top\">\n                        <ng-content select=\"div[slot=bottom-end-top]\"></ng-content>\n                    </div>\n                    <div class=\"bottom\">\n                        <ng-content select=\"div[slot=bottom-end-bottom]\"></ng-content>\n                    </div>\n                </div>\n            </ion-col>\n        </ion-row>\n        <ng-content></ng-content>\n    </div>\n\n    <div class=\"message-detail-icon\">\n        <ion-icon name=\"chevron-forward-outline\" [hidden]=\"!detail\"></ion-icon>\n    </div>\n</div>",
            styles: [":host .message-item-row{display:flex;justify-content:space-between;align-items:center;padding:16px 0 16px 16px}:host .message-item-row ion-col{padding:0}:host .message-item-row-content{width:100%}:host .message-item-row-content-top{display:flex;justify-content:space-between;margin-bottom:8px}:host .message-item-row-content-top .top-icon-container{font-size:x-large;width:36px;height:36px;background:#d3d3d3;border-radius:8px}:host .message-item-row-content-top ion-row{width:100%}:host .message-item-row-content-top .col-left{white-space:normal;padding-left:8px}:host .message-item-row-content-top .col-right{padding-top:3px;font-size:smaller;text-align:right}:host .bottom-row .left-col{margin-top:8px}:host .bottom-row .right-col .right{display:flex;height:100%;flex-wrap:wrap;align-content:space-between;justify-content:space-evenly}:host .bottom-row .right-col .right .top{width:100%;display:flex;justify-content:flex-end}:host .bottom-row .right-col .right .bottom{width:100%;text-align:right}:host .message-detail-icon{min-width:32px}:host .message-detail-icon ion-icon{margin-left:8px;font-size:x-large}"]
        })
    ], MessageItemComponent);
    return MessageItemComponent;
}());

var MESSAGES_REPOSITORY = new InjectionToken('messagesRepository');

var MessagesRepository = /** @class */ (function () {
    function MessagesRepository(appSettings, httpClient) {
        this.appSettings = appSettings;
        this.httpClient = httpClient;
    }
    MessagesRepository.prototype.getLeads = function () {
        return this.httpClient.get(this.getBaseUrl() + "/leads");
    };
    MessagesRepository.prototype.setLeadAsRead = function (id) {
        var data = { lead_id: id };
        var urlSearchParams = new URLSearchParams();
        Object.keys(data).forEach(function (key, i) {
            urlSearchParams.append(key, data[key]);
        });
        var body = urlSearchParams.toString();
        return this.httpClient.post(this.getBaseUrl() + "/leads/mark_message_as_read", body);
    };
    MessagesRepository.prototype.getReviews = function () {
        return this.httpClient.get(this.getBaseUrl() + "/gmb/reviews");
    };
    MessagesRepository.prototype.getReview = function (reviewId) {
        return this.httpClient.get(this.getBaseUrl() + "/gmb/review/" + reviewId);
    };
    MessagesRepository.prototype.upsertReviewReply = function (reviewId, comment) {
        var params = new HttpParams();
        params = params.append('comment', comment);
        return this.httpClient.post(this.getBaseUrl() + "/gmb/upsertReview/" + reviewId, params.toString());
    };
    MessagesRepository.prototype.deleteReviewReply = function (reviewId) {
        return this.httpClient.delete(this.getBaseUrl() + "/gmb/deleteReview/" + reviewId);
    };
    MessagesRepository.prototype.getBaseUrl = function () {
        return this.appSettings.baseUrl() + "/api/" + this.appSettings.instance() + "/v1";
    };
    MessagesRepository.ctorParameters = function () { return [
        { type: AbstractAppConfigService, decorators: [{ type: Inject, args: [APP_CONFIG_SERVICE,] }] },
        { type: HttpClient }
    ]; };
    MessagesRepository = __decorate([
        Injectable(),
        __param(0, Inject(APP_CONFIG_SERVICE))
    ], MessagesRepository);
    return MessagesRepository;
}());

var MESSAGES_SERVICE = new InjectionToken('messagesService');

/**
 * Superclass for any type of message:
 * Lead and Review by now
 */
var MessageBaseModel = /** @class */ (function () {
    function MessageBaseModel(data) {
        this.id = data.id;
        this.senderName = data.senderName;
        this.message = data.message;
        this.createTime = data.createTime;
        this.source = data.source;
    }
    return MessageBaseModel;
}());
var MessageSource;
(function (MessageSource) {
    MessageSource[MessageSource["EMAIL"] = 0] = "EMAIL";
    MessageSource[MessageSource["GMB_MESSAGES"] = 1] = "GMB_MESSAGES";
    MessageSource[MessageSource["GMB_REVIEW"] = 2] = "GMB_REVIEW";
    MessageSource[MessageSource["CHAT_WEBSITE"] = 3] = "CHAT_WEBSITE";
    MessageSource[MessageSource["UNKNOWN"] = 4] = "UNKNOWN";
})(MessageSource || (MessageSource = {}));

var LeadsModel = /** @class */ (function () {
    function LeadsModel(data) {
        this.totals = data.totals;
        this.periods = data.periods;
        this.leads = data.leads;
        this.chart = data.chart;
    }
    LeadsModel.fromApiResponse = function (data) {
        var leads = data.messages.map(function (lead) { return LeadModel.fromApiResponse(lead); });
        return new LeadsModel({
            totals: data.totals,
            periods: data.periods,
            leads: leads,
            chart: data.chart
        });
    };
    LeadsModel.empty = function () {
        return new LeadsModel({
            totals: {
                total: 0,
                seen: 0,
                new: 0
            },
            periods: null,
            leads: [],
            chart: []
        });
    };
    return LeadsModel;
}());
var LeadModel = /** @class */ (function (_super) {
    __extends(LeadModel, _super);
    function LeadModel(data) {
        var _this = _super.call(this, {
            id: data.id,
            senderName: data.senderName,
            message: data.message,
            createTime: data.createTime,
            source: data.source
        }) || this;
        _this.email = data.email;
        _this.phone = data.phone;
        _this.formattedDate = data.formattedDate;
        _this.date = data.date;
        _this.hour = data.hour;
        _this.timeAgo = data.timeAgo;
        _this.leadTimeAgo = data.leadTimeAgo;
        _this.readStatus = data.readStatus;
        var shortMsg = (data.shortMessage || data.message.substring(0, 30));
        _this.shortMessage = shortMsg.length >= 30 ? shortMsg.concat('...') : shortMsg;
        _this.source = data.source;
        return _this;
    }
    LeadModel.fromApiResponse = function (data) {
        return new LeadModel({
            id: +data.id,
            senderName: data.name || data.data_name,
            message: data.message || data.data_message,
            email: data.mail || data.data_email,
            phone: data.phone || data.data_phone,
            createTime: data.timestamp,
            formattedDate: data.formatted_date,
            date: data.date || data.created_at,
            hour: data.hour,
            timeAgo: data.time_ago,
            leadTimeAgo: data.lead_time_ago,
            readStatus: data.read_status === '1',
            shortMessage: data.short_message,
            source: MessageSource.EMAIL
        });
    };
    LeadModel.empty = function () {
        return new LeadModel({
            id: null,
            senderName: '',
            createTime: null,
            message: '',
            email: null,
            phone: null,
            formattedDate: null,
            date: null,
            hour: null,
            timeAgo: null,
            leadTimeAgo: null,
            readStatus: null,
            shortMessage: null,
            source: MessageSource.UNKNOWN,
            favorite: false
        });
    };
    return LeadModel;
}(MessageBaseModel));

var MessagesPageModel = /** @class */ (function () {
    function MessagesPageModel(data) {
        this.stats = data.stats;
        this.messages = data.messages;
    }
    MessagesPageModel.empty = function () {
        return new MessagesPageModel({
            stats: {
                leads: {
                    total: 0,
                    read: 0,
                    new: 0
                },
                reviews: {
                    averageRating: 0,
                    totalReviewCount: 0
                }
            },
            messages: [],
        });
    };
    return MessagesPageModel;
}());

var ReviewsModel = /** @class */ (function () {
    function ReviewsModel(data) {
        this.averageRating = data.averageRating;
        this.reviews = data.reviews;
        this.totalReviewCount = data.totalReviewCount;
    }
    ReviewsModel.fromApiResponse = function (data) {
        var reviewList = (!Array.isArray(data.reviews) ? [] : data.reviews).map(function (review) { return ReviewModel.fromApiResponse({
            id: review.id,
            comment: review.comment,
            createTime: review.createTime,
            reviewer: review.reviewer,
            starRating: review.starRating,
            updateTime: review.updateTime,
            reviewReply: review.reviewReply
        }); });
        return new ReviewsModel({
            averageRating: data.averageRating || 0,
            reviews: reviewList,
            totalReviewCount: data.totalReviewCount || 0
        });
    };
    ReviewsModel.prototype.empty = function () {
        return new ReviewsModel({
            averageRating: null,
            reviews: null,
            totalReviewCount: null
        });
    };
    return ReviewsModel;
}());
var ReviewModel = /** @class */ (function (_super) {
    __extends(ReviewModel, _super);
    function ReviewModel(data) {
        var _this = _super.call(this, {
            id: data.id,
            senderName: data.senderName,
            message: data.message,
            createTime: data.createTime,
            source: data.source
        }) || this;
        _this.senderPhotoUrl = data.senderPhotoUrl;
        _this.starRating = data.starRating;
        _this.updateTime = data.updateTime;
        _this.reviewReply = data.reviewReply;
        return _this;
    }
    ReviewModel.fromApiResponse = function (data) {
        return new ReviewModel({
            // base class properties:
            id: data.id,
            senderName: data.reviewer.displayName,
            message: data.comment,
            createTime: data.createTime,
            source: MessageSource.GMB_REVIEW,
            // own (extended) properties:
            senderPhotoUrl: data.reviewer.profilePhotoUrl,
            starRating: data.starRating,
            updateTime: data.updateTime,
            reviewReply: data.reviewReply
        });
    };
    return ReviewModel;
}(MessageBaseModel));

var MessagesService = /** @class */ (function () {
    function MessagesService(repository) {
        this.repository = repository;
    }
    MessagesService.prototype.getMessages = function (sorting) {
        var leadsError;
        var ReviewsError;
        return forkJoin([
            this.getLeads().pipe(catchError(function (err) { leadsError = err; return of(null); })),
            this.getReviews().pipe(catchError(function (err) { ReviewsError = err; return of(null); })),
        ]).pipe(map(function (_a) {
            var _b = __read(_a, 2), leadsData = _b[0], reviewsData = _b[1];
            return new MessagesPageModel({
                stats: {
                    leads: {
                        error: leadsError,
                        total: leadsError ? 0 : leadsData.totals.total,
                        read: leadsError ? 0 : leadsData.totals.seen,
                        new: leadsError ? 0 : leadsData.totals.new
                    },
                    reviews: {
                        error: ReviewsError,
                        averageRating: ReviewsError ? 0 : reviewsData.averageRating,
                        totalReviewCount: ReviewsError ? 0 : reviewsData.totalReviewCount
                    }
                },
                messages: __spread((leadsError ? [] : __spread(leadsData.leads)), (ReviewsError ? [] : __spread(reviewsData.reviews))).sort(function (a, b) {
                    return (sorting === 'ASC' ?
                        b.createTime.localeCompare(a.createTime) :
                        a.createTime.localeCompare(b.createTime));
                }),
            });
        }));
    };
    MessagesService.prototype.getLeads = function () {
        return this.repository.getLeads().pipe(map(function (response) {
            return LeadsModel.fromApiResponse(response.data);
        }));
    };
    MessagesService.prototype.setLeadAsRead = function (id) {
        return this.repository.setLeadAsRead(id).pipe(map(function (response) {
            return LeadModel.fromApiResponse(response.data);
        }));
    };
    MessagesService.prototype.getReviews = function () {
        return this.repository.getReviews().pipe(map(function (response) {
            return ReviewsModel.fromApiResponse(response.data);
        }));
    };
    MessagesService.prototype.getReview = function (reviewId) {
        return this.repository.getReview(reviewId).pipe(map(function (response) {
            return ReviewModel.fromApiResponse(response.data);
        }));
    };
    MessagesService.prototype.upsertReviewReply = function (review, comment) {
        return this.repository.upsertReviewReply(review.id, comment).pipe(map(function (response) {
            return new ReviewModel({
                id: review.id,
                senderName: review.senderName,
                message: review.message,
                createTime: review.createTime,
                source: review.source,
                senderPhotoUrl: review.senderPhotoUrl,
                starRating: review.starRating,
                updateTime: review.updateTime,
                reviewReply: {
                    comment: comment,
                    updateTime: response.data.updateTime,
                }
            });
        }));
    };
    MessagesService.prototype.deleteReviewReply = function (review) {
        return this.repository.deleteReviewReply(review.id).pipe(map(function (response) {
            if (response.status === 'success') {
                return new ReviewModel({
                    id: review.id,
                    senderName: review.senderName,
                    message: review.message,
                    createTime: review.createTime,
                    source: review.source,
                    senderPhotoUrl: review.senderPhotoUrl,
                    starRating: review.starRating,
                    updateTime: review.updateTime
                });
            }
            else {
                throw Error(response.message || 'Unknown error');
            }
        }));
    };
    MessagesService.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [MESSAGES_REPOSITORY,] }] }
    ]; };
    MessagesService.ɵprov = ɵɵdefineInjectable({ factory: function MessagesService_Factory() { return new MessagesService(ɵɵinject(MESSAGES_REPOSITORY)); }, token: MessagesService, providedIn: "root" });
    MessagesService = __decorate([
        Injectable({
            providedIn: 'root'
        }),
        __param(0, Inject(MESSAGES_REPOSITORY))
    ], MessagesService);
    return MessagesService;
}());

var MessagesActionTypes;
(function (MessagesActionTypes) {
    MessagesActionTypes["GetMessagesBegin"] = "[Messages] Get Messages begin";
    MessagesActionTypes["GetMessagesSuccess"] = "[Messages] Get Messages success";
    MessagesActionTypes["GetMessagesFail"] = "[Messages] Get Messages failure";
    MessagesActionTypes["SetLeadAsReadBegin"] = "[Messages] Set Lead as Read begin";
    MessagesActionTypes["SetLeadAsReadSuccess"] = "[Messages] Set Messages as Read success";
    MessagesActionTypes["SetLeadAsReadFail"] = "[Messages] Set Messages as Read failure";
    MessagesActionTypes["UpsertReviewReplyBegin"] = "[Messages] Upsert Review Reply begin";
    MessagesActionTypes["UpsertReviewReplySuccess"] = "[Messages] Upsert Review Reply success";
    MessagesActionTypes["UpsertReviewReplyFail"] = "[Messages] Upsert Review Reply failure";
    MessagesActionTypes["DeleteReviewReplyBegin"] = "[Messages] Delete Review Reply begin";
    MessagesActionTypes["DeleteReviewReplySuccess"] = "[Messages] Delete Review Reply success";
    MessagesActionTypes["DeleteReviewReplyFail"] = "[Messages] Delete Review Reply failure";
    MessagesActionTypes["FilterMessagesByReadStatusBegin"] = "[Messages] Filter message by Read Status begin";
    MessagesActionTypes["FilterMessagesByReadStatusSuccess"] = "[Messages] Filter message success";
    MessagesActionTypes["FilterMessagesBySourceBegin"] = "[Messages] Filter message by Source begin";
    MessagesActionTypes["FilterMessagesSourceSuccess"] = "[Messages] Filter message by Source success";
    MessagesActionTypes["SortMessagesByDateBegin"] = "[Messages] Sort message by Date begin";
    MessagesActionTypes["SortMessagesByDateSuccess"] = "[Messages] Sort message by Date success";
    MessagesActionTypes["SelectMessage"] = "[Messages] Select message";
})(MessagesActionTypes || (MessagesActionTypes = {}));
// GET Messages from remote API
var GetMessagesBeginAction = createAction(MessagesActionTypes.GetMessagesBegin, props());
var GetMessagesSuccessAction = createAction(MessagesActionTypes.GetMessagesSuccess, props());
var GetMessagesFailAction = createAction(MessagesActionTypes.GetMessagesFail, props());
// SET AS READ
var SetLeadAsReadBeginAction = createAction(MessagesActionTypes.SetLeadAsReadBegin, props());
var SetLeadAsReadSuccessAction = createAction(MessagesActionTypes.SetLeadAsReadSuccess, props());
var SetLeadAsReadFailAction = createAction(MessagesActionTypes.SetLeadAsReadFail, props());
var UpsertReviewReplyBeginAction = createAction(MessagesActionTypes.UpsertReviewReplyBegin, props());
var UpsertReviewReplySuccessAction = createAction(MessagesActionTypes.UpsertReviewReplySuccess, props());
var UpsertReviewReplyFaliAction = createAction(MessagesActionTypes.UpsertReviewReplyFail, props());
var DeleteReviewReplyBeginAction = createAction(MessagesActionTypes.DeleteReviewReplyBegin, props());
var DeleteReviewReplySuccessAction = createAction(MessagesActionTypes.DeleteReviewReplySuccess, props());
var DeleteReviewReplyFaliAction = createAction(MessagesActionTypes.DeleteReviewReplyFail, props());
// FILTERING
var FilterMessagesByReadStatusBeginAction = createAction(MessagesActionTypes.FilterMessagesByReadStatusBegin, props());
var FilterMessagesByReadStatusSuccessAction = createAction(MessagesActionTypes.FilterMessagesByReadStatusSuccess, props());
var FilterMessagesBySourceBeginAction = createAction(MessagesActionTypes.FilterMessagesBySourceBegin, props());
var FilterMessagesBySourceSuccessAction = createAction(MessagesActionTypes.FilterMessagesSourceSuccess, props());
var SelectMessageAction = createAction(MessagesActionTypes.SelectMessage, props());

var MessagesEffects = /** @class */ (function () {
    function MessagesEffects(actions$, store$, service) {
        var _this = this;
        this.actions$ = actions$;
        this.store$ = store$;
        this.service = service;
        this.load$ = createEffect(function () { return _this.actions$.pipe(ofType(MessagesActionTypes.GetMessagesBegin), switchMap(function (action) {
            return _this.service.getMessages(action.sorting).pipe(map(function (data) { return GetMessagesSuccessAction({ data: data }); }), catchError(function (error) {
                console.error('Couldn\'t get messages', error);
                return of(GetMessagesFailAction({ errors: error }));
            }));
        })); });
        this.setAsRead$ = createEffect(function () { return _this.actions$.pipe(ofType(MessagesActionTypes.SetLeadAsReadBegin), switchMap(function (action) {
            return _this.service.setLeadAsRead(action.id).pipe(map(function (message) { return SetLeadAsReadSuccessAction({ message: message }); }), catchError(function (error) {
                console.error('Couldn\'t set message as read', error);
                return of(SetLeadAsReadFailAction({ errors: error }));
            }));
        })); });
        this.filter$ = createEffect(function () { return _this.actions$.pipe(ofType(MessagesActionTypes.FilterMessagesByReadStatusBegin), withLatestFrom(_this.store$), switchMap(function (_a) {
            var _b = __read(_a, 2), action = _b[0], store = _b[1];
            var messageType = action.messageType;
            var messageList = store.messages.pageData.messages.filter(function (item) { return store.messages.filteredSources.includes(item.source); });
            messageList = messageType === 'ALL' ? messageList :
                messageList.filter(function (item) {
                    if (messageType === 'READ' && item.readStatus === undefined) {
                        return true;
                    }
                    return item.readStatus === (messageType === 'READ' ? true : false);
                });
            return of(FilterMessagesByReadStatusSuccessAction({ messageList: messageList }));
        })); });
        this.sortAndFilter$ = createEffect(function () { return _this.actions$.pipe(ofType(MessagesActionTypes.FilterMessagesBySourceBegin), withLatestFrom(_this.store$), switchMap(function (_a) {
            var _b = __read(_a, 2), action = _b[0], store = _b[1];
            var sources = action.sources;
            var sorting = action.sorting;
            var sortedMessages = store.messages.pageData.messages
                .map(function (m) { return m; }) // <-- because pageData.messages is READ ONLY!!
                .sort(function (a, b) {
                return (sorting === 'ASC' ?
                    b.createTime.localeCompare(a.createTime) :
                    a.createTime.localeCompare(b.createTime));
            });
            var filteredMessages = sortedMessages.filter(function (item) { return sources.includes(item.source); });
            return of(FilterMessagesBySourceSuccessAction({
                sorting: sorting,
                sources: sources,
                filteredMessages: filteredMessages
            }));
        })); });
        this.upsertReviewReply$ = createEffect(function () { return _this.actions$.pipe(ofType(UpsertReviewReplyBeginAction), switchMap(function (action) {
            return _this.service.upsertReviewReply(action.review, action.comment).pipe(map(function (review) { return UpsertReviewReplySuccessAction({ review: review }); }), catchError(function (error) {
                console.error('Couldn\'t upsert review reply', error);
                return of(UpsertReviewReplyFaliAction({ errors: error }));
            }));
        })); });
        this.deleteReviewReply$ = createEffect(function () { return _this.actions$.pipe(ofType(DeleteReviewReplyBeginAction), switchMap(function (action) {
            return _this.service.deleteReviewReply(action.review).pipe(map(function (review) { return DeleteReviewReplySuccessAction({ review: review }); }), catchError(function (error) {
                console.error('Couldn\'t delete review reply', error);
                return of(DeleteReviewReplyFaliAction({ errors: error }));
            }));
        })); });
    }
    MessagesEffects.ctorParameters = function () { return [
        { type: Actions },
        { type: Store },
        { type: undefined, decorators: [{ type: Inject, args: [MESSAGES_SERVICE,] }] }
    ]; };
    MessagesEffects = __decorate([
        Injectable(),
        __param(2, Inject(MESSAGES_SERVICE))
    ], MessagesEffects);
    return MessagesEffects;
}());

var initialState = {
    isLoading: false,
    hasBeenFetched: false,
    pageData: MessagesPageModel.empty(),
    filteredSources: [],
    filteredItems: [],
    sorting: 'ASC',
    selectedId: null,
    error: null,
    success: null
};
var ɵ0 = function (state) { return (__assign(__assign({}, state), { error: null, success: null, isLoading: true })); }, ɵ1 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, hasBeenFetched: true, pageData: __assign(__assign({}, state.pageData), { stats: action.data.stats, messages: action.data.messages }), error: null, success: { after: getAfterActionType(action.type) } })); }, ɵ2 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, hasBeenFetched: true, pageData: __assign(__assign({}, state.pageData), { messages: state.pageData.messages.map(function (m) {
            return m.id === action.review.id ? action.review : m;
        }) }), error: null, success: { after: getAfterActionType(action.type) } })); }, ɵ3 = function (state, action) { return (__assign(__assign({}, state), { pageData: __assign(__assign({}, state.pageData), { stats: __assign(__assign({}, state.pageData.stats), { leads: __assign(__assign({}, state.pageData.stats.leads), { read: state.pageData.stats.leads.read + 1, new: state.pageData.stats.leads.new - 1 }) }), messages: __spread((function (ml) {
            var tmp = __spread(ml);
            var idx = ml.findIndex(function (m) { return m.id === action.message.id; });
            if (idx !== -1) {
                tmp.splice(idx, 1, action.message);
            }
            return tmp;
        })(state.pageData.messages)) }), error: null, success: { after: getAfterActionType(action.type) } })); }, ɵ4 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, error: { after: getAfterActionType(action.type), error: action.errors } })); }, ɵ5 = function (state, action) { return (__assign(__assign({}, state), { error: { after: getAfterActionType(action.type), error: action.errors } })); }, ɵ6 = function (state, action) { return (__assign(__assign({}, state), { filteredItems: action.messageList, success: null })); }, ɵ7 = function (state, action) { return (__assign(__assign({}, state), { sorting: action.sorting, filteredSources: action.sources, filteredItems: action.filteredMessages, success: null })); }, ɵ8 = function (state, action) { return (__assign(__assign({}, state), { selectedId: action.messageId, success: null })); };
var reducer = createReducer(initialState, 
// On Begin Actions
on(GetMessagesBeginAction, UpsertReviewReplyBeginAction, DeleteReviewReplyBeginAction, ɵ0), 
// ON Success Actions
on(GetMessagesSuccessAction, ɵ1), on(UpsertReviewReplySuccessAction, DeleteReviewReplySuccessAction, ɵ2), on(SetLeadAsReadSuccessAction, ɵ3), 
// ON Fail Actions
on(GetMessagesFailAction, UpsertReviewReplyFaliAction, DeleteReviewReplyFaliAction, ɵ4), on(SetLeadAsReadFailAction, ɵ5), 
// FILTER
on(FilterMessagesByReadStatusSuccessAction, ɵ6), on(FilterMessagesBySourceSuccessAction, ɵ7), 
// SELECT
on(SelectMessageAction, ɵ8));
function getAfterActionType(type) {
    var action;
    switch (type) {
        case MessagesActionTypes.GetMessagesSuccess:
        case MessagesActionTypes.GetMessagesFail:
            action = 'GET';
            break;
        case MessagesActionTypes.SetLeadAsReadSuccess:
        case MessagesActionTypes.SetLeadAsReadFail:
            action = 'SET_LEAD_READ';
            break;
        case MessagesActionTypes.UpsertReviewReplySuccess:
        case MessagesActionTypes.UpsertReviewReplyFail:
            action = 'UPSERT_REVIEW';
            break;
        case MessagesActionTypes.DeleteReviewReplyFail:
        case MessagesActionTypes.DeleteReviewReplySuccess:
            action = 'DELETE_REVIEW';
            break;
        default:
            action = 'UNKNOWN';
    }
    return action;
}
function messagesReducer(state, action) {
    return reducer(state, action);
}

var getMessagesState = createFeatureSelector('messages');
var stateGetPageData = function (state) { return state.pageData; };
var stateGetIsLoading = function (state) { return state.isLoading; };
var stateGetMessages = function (state) { return state.pageData.messages; };
var stateGetLeads = function (state) { return state.pageData.messages.filter(function (m) { return m.source === MessageSource.EMAIL; }); };
var stateGetReviews = function (state) { return state.pageData.messages.filter(function (m) { return m.source === MessageSource.GMB_REVIEW; }); };
var stateGetFilteredItems = function (state) { return state.filteredItems; };
var ɵ0$1 = function (state) { return state; };
var getMessagesPageState = createSelector(getMessagesState, ɵ0$1);
var getMessagesPageData = createSelector(getMessagesState, stateGetPageData);
var getMessages = createSelector(getMessagesPageState, stateGetMessages);
var getLeads = createSelector(getMessagesPageState, stateGetLeads);
var getReviews = createSelector(getMessagesPageState, stateGetReviews);
var getIsLoading = createSelector(getMessagesPageState, stateGetIsLoading);
var ɵ1$1 = function (state) { return state.error; };
var getError = createSelector(getMessagesPageState, ɵ1$1);
var ɵ2$1 = function (state) { return state.success; };
var getSuccess = createSelector(getMessagesPageState, ɵ2$1);
var getFilteredMessages = createSelector(getMessagesPageState, stateGetFilteredItems);
var ɵ3$1 = function (state) { return state.pageData.messages.filter(function (m) { return m.id === state.selectedId; })[0]; };
var getMessageById = createSelector(getMessagesPageState, ɵ3$1);
var ɵ4$1 = function (state) { return state.hasBeenFetched; };
var hasBeenFetched = createSelector(getMessagesPageState, ɵ4$1);

var MessagesStore = /** @class */ (function () {
    function MessagesStore(store) {
        this.store = store;
    }
    Object.defineProperty(MessagesStore.prototype, "Loading$", {
        get: function () { return this.store.select(getIsLoading); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MessagesStore.prototype, "Error$", {
        get: function () {
            return this.store.select(getError);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MessagesStore.prototype, "Success$", {
        get: function () {
            return this.store.select(getSuccess);
        },
        enumerable: true,
        configurable: true
    });
    MessagesStore.prototype.loadMessagesPage = function (sorting) {
        return this.store.dispatch(GetMessagesBeginAction({ sorting: sorting }));
    };
    Object.defineProperty(MessagesStore.prototype, "MessagesPageData$", {
        get: function () {
            return this.store.select(getMessagesPageData);
        },
        enumerable: true,
        configurable: true
    });
    MessagesStore.prototype.filterMessagesByReadStatus = function (criteria) {
        this.store.dispatch(FilterMessagesByReadStatusBeginAction({ messageType: criteria }));
    };
    MessagesStore.prototype.filterMessagesBySource = function (sorting, sources) {
        if (sorting === void 0) { sorting = 'ASC'; }
        this.store.dispatch(FilterMessagesBySourceBeginAction({ sorting: sorting, sources: sources }));
    };
    Object.defineProperty(MessagesStore.prototype, "FilteredMessages$", {
        get: function () {
            return this.store.select(getFilteredMessages);
        },
        enumerable: true,
        configurable: true
    });
    MessagesStore.prototype.MessageById$ = function (messageId) {
        this.store.dispatch(SelectMessageAction({ messageId: messageId }));
        return this.store.select(getMessageById);
    };
    Object.defineProperty(MessagesStore.prototype, "Leads$", {
        get: function () {
            return this.store.select(getLeads);
        },
        enumerable: true,
        configurable: true
    });
    MessagesStore.prototype.setLeadAsRead = function (id) {
        this.store.dispatch(SetLeadAsReadBeginAction({ id: id }));
    };
    Object.defineProperty(MessagesStore.prototype, "Reviews$", {
        get: function () {
            return this.store.select(getReviews);
        },
        enumerable: true,
        configurable: true
    });
    MessagesStore.prototype.upsertReviewReply = function (review, comment) {
        this.store.dispatch(UpsertReviewReplyBeginAction({ review: review, comment: comment }));
    };
    MessagesStore.prototype.deleteReviewReply = function (review) {
        this.store.dispatch(DeleteReviewReplyBeginAction({ review: review }));
    };
    Object.defineProperty(MessagesStore.prototype, "HasBeenFetched$", {
        get: function () {
            return this.store.select(hasBeenFetched);
        },
        enumerable: true,
        configurable: true
    });
    MessagesStore.ctorParameters = function () { return [
        { type: Store }
    ]; };
    MessagesStore = __decorate([
        Injectable()
    ], MessagesStore);
    return MessagesStore;
}());

var MessagesCoreModule = /** @class */ (function () {
    function MessagesCoreModule() {
    }
    MessagesCoreModule_1 = MessagesCoreModule;
    MessagesCoreModule.forRoot = function (config) {
        return {
            ngModule: MessagesCoreModule_1,
            providers: __spread([
                { provide: MESSAGES_SERVICE, useClass: MessagesService },
                { provide: MESSAGES_REPOSITORY, useClass: MessagesRepository }
            ], config.providers, [
                MessagesStore
            ])
        };
    };
    var MessagesCoreModule_1;
    MessagesCoreModule = MessagesCoreModule_1 = __decorate([
        NgModule({
            declarations: [MessageItemComponent],
            imports: [
                HttpClientModule,
                StoreModule.forFeature('messages', messagesReducer),
                EffectsModule.forFeature([MessagesEffects]),
                CommonModule,
                FormsModule,
                IonicModule
            ],
            exports: [
                CommonModule,
                FormsModule,
                MessageItemComponent,
            ]
        })
    ], MessagesCoreModule);
    return MessagesCoreModule;
}());

/*
 * Public API Surface of messages
 */

/**
 * Generated bundle index. Do not edit.
 */

export { DeleteReviewReplyBeginAction, DeleteReviewReplyFaliAction, DeleteReviewReplySuccessAction, FilterMessagesByReadStatusBeginAction, FilterMessagesByReadStatusSuccessAction, FilterMessagesBySourceBeginAction, FilterMessagesBySourceSuccessAction, GetMessagesBeginAction, GetMessagesFailAction, GetMessagesSuccessAction, LeadModel, LeadsModel, MESSAGES_REPOSITORY, MESSAGES_SERVICE, MessageBaseModel, MessageSource, MessagesActionTypes, MessagesCoreModule, MessagesEffects, MessagesPageModel, MessagesRepository, MessagesService, MessagesStore, ReviewModel, ReviewsModel, SelectMessageAction, SetLeadAsReadBeginAction, SetLeadAsReadFailAction, SetLeadAsReadSuccessAction, UpsertReviewReplyBeginAction, UpsertReviewReplyFaliAction, UpsertReviewReplySuccessAction, getError, getFilteredMessages, getIsLoading, getLeads, getMessageById, getMessages, getMessagesPageData, getMessagesPageState, getMessagesState, getReviews, getSuccess, hasBeenFetched, initialState, messagesReducer, stateGetFilteredItems, stateGetIsLoading, stateGetLeads, stateGetMessages, stateGetPageData, stateGetReviews, ɵ0$1 as ɵ0, ɵ1$1 as ɵ1, ɵ2$1 as ɵ2, ɵ3$1 as ɵ3, ɵ4$1 as ɵ4, MessageItemComponent as ɵa };
//# sourceMappingURL=boxx-messages-core.js.map
