import { Observable } from 'rxjs';
import { LeadModel, LeadsModel } from '../models/Lead.model';
import { MessagesPageModel } from '../models/MessagePage.model';
import { ReviewModel, ReviewsModel } from '../models/Review.model';
import { IMessagesRepository } from '../repositories/IMessages.repository';
import { IMessagesService } from './IMessages.service';
export declare class MessagesService implements IMessagesService {
    private repository;
    constructor(repository: IMessagesRepository);
    getMessages(sorting?: 'ASC' | 'DESC'): Observable<MessagesPageModel>;
    getLeads(): Observable<LeadsModel>;
    setLeadAsRead(id: number): Observable<LeadModel>;
    getReviews(): Observable<ReviewsModel>;
    getReview(reviewId: string): Observable<ReviewModel>;
    upsertReviewReply(review: ReviewModel, comment: string): Observable<ReviewModel>;
    deleteReviewReply(review: ReviewModel): Observable<ReviewModel>;
}
