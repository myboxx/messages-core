import { InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';
import { IHttpBasicResponse } from '@boxx/core';
import { ILeadApi, ILeadsApi } from '../models/Leads-api.properties';
import { IReviewApi, IReviewsApi, IReviewUpsertApi } from '../models/Reviews-api.properties';
export interface IMessagesRepository {
    getLeads(): Observable<IHttpBasicResponse<ILeadsApi>>;
    setLeadAsRead(id: number): Observable<IHttpBasicResponse<ILeadApi>>;
    getReviews(): Observable<IHttpBasicResponse<IReviewsApi>>;
    getReview(reviewId: string): Observable<IHttpBasicResponse<IReviewApi>>;
    upsertReviewReply(reviewId: string, comment: string): Observable<IHttpBasicResponse<IReviewUpsertApi>>;
    deleteReviewReply(reviewId: string): Observable<IHttpBasicResponse<null>>;
}
export declare const MESSAGES_REPOSITORY: InjectionToken<IMessagesRepository>;
