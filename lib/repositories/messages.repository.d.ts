import { HttpClient } from '@angular/common/http';
import { AbstractAppConfigService, IHttpBasicResponse } from '@boxx/core';
import { Observable } from 'rxjs';
import { ILeadApi, ILeadsApi } from '../models/Leads-api.properties';
import { IReviewApi, IReviewsApi, IReviewUpsertApi } from '../models/Reviews-api.properties';
import { IMessagesRepository } from './IMessages.repository';
export declare class MessagesRepository implements IMessagesRepository {
    private appSettings;
    private httpClient;
    constructor(appSettings: AbstractAppConfigService, httpClient: HttpClient);
    getLeads(): Observable<IHttpBasicResponse<ILeadsApi>>;
    setLeadAsRead(id: number): Observable<IHttpBasicResponse<ILeadApi>>;
    getReviews(): Observable<IHttpBasicResponse<IReviewsApi>>;
    getReview(reviewId: string): Observable<IHttpBasicResponse<IReviewApi>>;
    upsertReviewReply(reviewId: string, comment: string): Observable<IHttpBasicResponse<IReviewUpsertApi>>;
    deleteReviewReply(reviewId: string): Observable<IHttpBasicResponse<null>>;
    getBaseUrl(): string;
}
