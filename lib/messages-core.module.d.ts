import { ModuleWithProviders, Provider } from '@angular/core';
interface ModuleOptionsInterface {
    providers: Provider[];
}
export declare class MessagesCoreModule {
    static forRoot(config: ModuleOptionsInterface): ModuleWithProviders<MessagesCoreModule>;
}
export {};
