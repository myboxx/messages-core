import { LeadModel } from '../models/Lead.model';
import { MessageBaseModel, MessageSource, MESSAGE_TYPE } from '../models/Message.model';
import { MessagesPageModel } from '../models/MessagePage.model';
import { ReviewModel } from '../models/Review.model';
export declare enum MessagesActionTypes {
    GetMessagesBegin = "[Messages] Get Messages begin",
    GetMessagesSuccess = "[Messages] Get Messages success",
    GetMessagesFail = "[Messages] Get Messages failure",
    SetLeadAsReadBegin = "[Messages] Set Lead as Read begin",
    SetLeadAsReadSuccess = "[Messages] Set Messages as Read success",
    SetLeadAsReadFail = "[Messages] Set Messages as Read failure",
    UpsertReviewReplyBegin = "[Messages] Upsert Review Reply begin",
    UpsertReviewReplySuccess = "[Messages] Upsert Review Reply success",
    UpsertReviewReplyFail = "[Messages] Upsert Review Reply failure",
    DeleteReviewReplyBegin = "[Messages] Delete Review Reply begin",
    DeleteReviewReplySuccess = "[Messages] Delete Review Reply success",
    DeleteReviewReplyFail = "[Messages] Delete Review Reply failure",
    FilterMessagesByReadStatusBegin = "[Messages] Filter message by Read Status begin",
    FilterMessagesByReadStatusSuccess = "[Messages] Filter message success",
    FilterMessagesBySourceBegin = "[Messages] Filter message by Source begin",
    FilterMessagesSourceSuccess = "[Messages] Filter message by Source success",
    SortMessagesByDateBegin = "[Messages] Sort message by Date begin",
    SortMessagesByDateSuccess = "[Messages] Sort message by Date success",
    SelectMessage = "[Messages] Select message"
}
export declare const GetMessagesBeginAction: import("@ngrx/store").ActionCreator<MessagesActionTypes.GetMessagesBegin, (props: {
    sorting?: "ASC" | "DESC";
}) => {
    sorting?: "ASC" | "DESC";
} & import("@ngrx/store/src/models").TypedAction<MessagesActionTypes.GetMessagesBegin>>;
export declare const GetMessagesSuccessAction: import("@ngrx/store").ActionCreator<MessagesActionTypes.GetMessagesSuccess, (props: {
    data: MessagesPageModel;
}) => {
    data: MessagesPageModel;
} & import("@ngrx/store/src/models").TypedAction<MessagesActionTypes.GetMessagesSuccess>>;
export declare const GetMessagesFailAction: import("@ngrx/store").ActionCreator<MessagesActionTypes.GetMessagesFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<MessagesActionTypes.GetMessagesFail>>;
export declare const SetLeadAsReadBeginAction: import("@ngrx/store").ActionCreator<MessagesActionTypes.SetLeadAsReadBegin, (props: {
    id: number;
}) => {
    id: number;
} & import("@ngrx/store/src/models").TypedAction<MessagesActionTypes.SetLeadAsReadBegin>>;
export declare const SetLeadAsReadSuccessAction: import("@ngrx/store").ActionCreator<MessagesActionTypes.SetLeadAsReadSuccess, (props: {
    message: LeadModel;
}) => {
    message: LeadModel;
} & import("@ngrx/store/src/models").TypedAction<MessagesActionTypes.SetLeadAsReadSuccess>>;
export declare const SetLeadAsReadFailAction: import("@ngrx/store").ActionCreator<MessagesActionTypes.SetLeadAsReadFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<MessagesActionTypes.SetLeadAsReadFail>>;
export declare const UpsertReviewReplyBeginAction: import("@ngrx/store").ActionCreator<MessagesActionTypes.UpsertReviewReplyBegin, (props: {
    review: ReviewModel;
    comment: string;
}) => {
    review: ReviewModel;
    comment: string;
} & import("@ngrx/store/src/models").TypedAction<MessagesActionTypes.UpsertReviewReplyBegin>>;
export declare const UpsertReviewReplySuccessAction: import("@ngrx/store").ActionCreator<MessagesActionTypes.UpsertReviewReplySuccess, (props: {
    review: ReviewModel;
}) => {
    review: ReviewModel;
} & import("@ngrx/store/src/models").TypedAction<MessagesActionTypes.UpsertReviewReplySuccess>>;
export declare const UpsertReviewReplyFaliAction: import("@ngrx/store").ActionCreator<MessagesActionTypes.UpsertReviewReplyFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<MessagesActionTypes.UpsertReviewReplyFail>>;
export declare const DeleteReviewReplyBeginAction: import("@ngrx/store").ActionCreator<MessagesActionTypes.DeleteReviewReplyBegin, (props: {
    review: ReviewModel;
}) => {
    review: ReviewModel;
} & import("@ngrx/store/src/models").TypedAction<MessagesActionTypes.DeleteReviewReplyBegin>>;
export declare const DeleteReviewReplySuccessAction: import("@ngrx/store").ActionCreator<MessagesActionTypes.DeleteReviewReplySuccess, (props: {
    review: ReviewModel;
}) => {
    review: ReviewModel;
} & import("@ngrx/store/src/models").TypedAction<MessagesActionTypes.DeleteReviewReplySuccess>>;
export declare const DeleteReviewReplyFaliAction: import("@ngrx/store").ActionCreator<MessagesActionTypes.DeleteReviewReplyFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<MessagesActionTypes.DeleteReviewReplyFail>>;
export declare const FilterMessagesByReadStatusBeginAction: import("@ngrx/store").ActionCreator<MessagesActionTypes.FilterMessagesByReadStatusBegin, (props: {
    messageType: MESSAGE_TYPE;
}) => {
    messageType: MESSAGE_TYPE;
} & import("@ngrx/store/src/models").TypedAction<MessagesActionTypes.FilterMessagesByReadStatusBegin>>;
export declare const FilterMessagesByReadStatusSuccessAction: import("@ngrx/store").ActionCreator<MessagesActionTypes.FilterMessagesByReadStatusSuccess, (props: {
    messageList: MessageBaseModel[];
}) => {
    messageList: MessageBaseModel[];
} & import("@ngrx/store/src/models").TypedAction<MessagesActionTypes.FilterMessagesByReadStatusSuccess>>;
export declare const FilterMessagesBySourceBeginAction: import("@ngrx/store").ActionCreator<MessagesActionTypes.FilterMessagesBySourceBegin, (props: {
    sorting: "ASC" | "DESC";
    sources: MessageSource[];
}) => {
    sorting: "ASC" | "DESC";
    sources: MessageSource[];
} & import("@ngrx/store/src/models").TypedAction<MessagesActionTypes.FilterMessagesBySourceBegin>>;
export declare const FilterMessagesBySourceSuccessAction: import("@ngrx/store").ActionCreator<MessagesActionTypes.FilterMessagesSourceSuccess, (props: {
    sorting: "ASC" | "DESC";
    sources: MessageSource[];
    filteredMessages: MessageBaseModel[];
}) => {
    sorting: "ASC" | "DESC";
    sources: MessageSource[];
    filteredMessages: MessageBaseModel[];
} & import("@ngrx/store/src/models").TypedAction<MessagesActionTypes.FilterMessagesSourceSuccess>>;
export declare const SelectMessageAction: import("@ngrx/store").ActionCreator<MessagesActionTypes.SelectMessage, (props: {
    messageId: number;
}) => {
    messageId: number;
} & import("@ngrx/store/src/models").TypedAction<MessagesActionTypes.SelectMessage>>;
