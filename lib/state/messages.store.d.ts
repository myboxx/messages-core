import { Store } from '@ngrx/store';
import { MessageSource, MESSAGE_TYPE } from '../models/Message.model';
import { ReviewModel } from '../models/Review.model';
import * as fromReducer from './messages.reducer';
export declare class MessagesStore {
    store: Store<fromReducer.MessagesState>;
    constructor(store: Store<fromReducer.MessagesState>);
    get Loading$(): import("rxjs").Observable<boolean>;
    get Error$(): import("rxjs").Observable<import("../core/IStateErrorSuccess").IMessagesStateError>;
    get Success$(): import("rxjs").Observable<import("../core/IStateErrorSuccess").IMessagesStateSuccess>;
    loadMessagesPage(sorting?: 'ASC' | 'DESC'): void;
    get MessagesPageData$(): import("rxjs").Observable<import("../models/MessagePage.model").MessagesPageModel>;
    filterMessagesByReadStatus(criteria: MESSAGE_TYPE): void;
    filterMessagesBySource(sorting: 'ASC' | 'DESC', sources: MessageSource[]): void;
    get FilteredMessages$(): import("rxjs").Observable<import("../models/Message.model").MessageBaseModel[]>;
    MessageById$(messageId: number): import("rxjs").Observable<import("../models/Message.model").MessageBaseModel>;
    get Leads$(): import("rxjs").Observable<import("../models/Message.model").MessageBaseModel[]>;
    setLeadAsRead(id: number): void;
    get Reviews$(): import("rxjs").Observable<import("../models/Message.model").MessageBaseModel[]>;
    upsertReviewReply(review: ReviewModel, comment: string): void;
    deleteReviewReply(review: ReviewModel): void;
    get HasBeenFetched$(): import("rxjs").Observable<boolean>;
}
