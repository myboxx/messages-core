import * as fromReducer from './messages.reducer';
export declare const getMessagesState: import("@ngrx/store").MemoizedSelector<object, fromReducer.MessagesState, import("@ngrx/store").DefaultProjectorFn<fromReducer.MessagesState>>;
export declare const stateGetPageData: (state: fromReducer.MessagesState) => import("../models/MessagePage.model").MessagesPageModel;
export declare const stateGetIsLoading: (state: fromReducer.MessagesState) => boolean;
export declare const stateGetMessages: (state: fromReducer.MessagesState) => import("../models/Message.model").MessageBaseModel[];
export declare const stateGetLeads: (state: fromReducer.MessagesState) => import("../models/Message.model").MessageBaseModel[];
export declare const stateGetReviews: (state: fromReducer.MessagesState) => import("../models/Message.model").MessageBaseModel[];
export declare const stateGetFilteredItems: (state: fromReducer.MessagesState) => import("../models/Message.model").MessageBaseModel[];
export declare const getMessagesPageState: import("@ngrx/store").MemoizedSelector<object, fromReducer.MessagesState, import("@ngrx/store").DefaultProjectorFn<fromReducer.MessagesState>>;
export declare const getMessagesPageData: import("@ngrx/store").MemoizedSelector<object, import("../models/MessagePage.model").MessagesPageModel, import("@ngrx/store").DefaultProjectorFn<import("../models/MessagePage.model").MessagesPageModel>>;
export declare const getMessages: import("@ngrx/store").MemoizedSelector<object, import("../models/Message.model").MessageBaseModel[], import("@ngrx/store").DefaultProjectorFn<import("../models/Message.model").MessageBaseModel[]>>;
export declare const getLeads: import("@ngrx/store").MemoizedSelector<object, import("../models/Message.model").MessageBaseModel[], import("@ngrx/store").DefaultProjectorFn<import("../models/Message.model").MessageBaseModel[]>>;
export declare const getReviews: import("@ngrx/store").MemoizedSelector<object, import("../models/Message.model").MessageBaseModel[], import("@ngrx/store").DefaultProjectorFn<import("../models/Message.model").MessageBaseModel[]>>;
export declare const getIsLoading: import("@ngrx/store").MemoizedSelector<object, boolean, import("@ngrx/store").DefaultProjectorFn<boolean>>;
export declare const getError: import("@ngrx/store").MemoizedSelector<object, import("../core/IStateErrorSuccess").IMessagesStateError, import("@ngrx/store").DefaultProjectorFn<import("../core/IStateErrorSuccess").IMessagesStateError>>;
export declare const getSuccess: import("@ngrx/store").MemoizedSelector<object, import("../core/IStateErrorSuccess").IMessagesStateSuccess, import("@ngrx/store").DefaultProjectorFn<import("../core/IStateErrorSuccess").IMessagesStateSuccess>>;
export declare const getFilteredMessages: import("@ngrx/store").MemoizedSelector<object, import("../models/Message.model").MessageBaseModel[], import("@ngrx/store").DefaultProjectorFn<import("../models/Message.model").MessageBaseModel[]>>;
export declare const getMessageById: import("@ngrx/store").MemoizedSelector<object, import("../models/Message.model").MessageBaseModel, import("@ngrx/store").DefaultProjectorFn<import("../models/Message.model").MessageBaseModel>>;
export declare const hasBeenFetched: import("@ngrx/store").MemoizedSelector<object, boolean, import("@ngrx/store").DefaultProjectorFn<boolean>>;
