import { Action } from '@ngrx/store';
import { IMessagesStateError, IMessagesStateSuccess } from '../core/IStateErrorSuccess';
import { MessageBaseModel, MessageSource } from '../models/Message.model';
import { MessagesPageModel } from '../models/MessagePage.model';
export interface MessagesState {
    isLoading: boolean;
    hasBeenFetched: boolean;
    pageData: MessagesPageModel;
    filteredSources: MessageSource[];
    filteredItems: MessageBaseModel[];
    sorting: 'ASC' | 'DESC';
    selectedId: number | string;
    error: IMessagesStateError;
    success: IMessagesStateSuccess;
}
export declare const initialState: MessagesState;
export declare function messagesReducer(state: MessagesState | undefined, action: Action): MessagesState;
