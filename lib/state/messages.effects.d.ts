import { Actions } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { MessageSource } from '../models/Message.model';
import { IMessagesService } from '../services/IMessages.service';
import * as fromActions from './messages.actions';
import * as fromReducer from './messages.reducer';
export declare class MessagesEffects {
    private actions$;
    private store$;
    private service;
    load$: Observable<({
        data: import("../models/MessagePage.model").MessagesPageModel;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.MessagesActionTypes.GetMessagesSuccess>) | ({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.MessagesActionTypes.GetMessagesFail>)> & import("@ngrx/effects").CreateEffectMetadata;
    setAsRead$: Observable<({
        message: import("../models/Lead.model").LeadModel;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.MessagesActionTypes.SetLeadAsReadSuccess>) | ({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.MessagesActionTypes.SetLeadAsReadFail>)> & import("@ngrx/effects").CreateEffectMetadata;
    filter$: Observable<{
        messageList: import("../models/Message.model").MessageBaseModel[];
    } & import("@ngrx/store/src/models").TypedAction<fromActions.MessagesActionTypes.FilterMessagesByReadStatusSuccess>> & import("@ngrx/effects").CreateEffectMetadata;
    sortAndFilter$: Observable<{
        sorting: "ASC" | "DESC";
        sources: MessageSource[];
        filteredMessages: import("../models/Message.model").MessageBaseModel[];
    } & import("@ngrx/store/src/models").TypedAction<fromActions.MessagesActionTypes.FilterMessagesSourceSuccess>> & import("@ngrx/effects").CreateEffectMetadata;
    upsertReviewReply$: Observable<({
        review: import("../models/Review.model").ReviewModel;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.MessagesActionTypes.UpsertReviewReplySuccess>) | ({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.MessagesActionTypes.UpsertReviewReplyFail>)> & import("@ngrx/effects").CreateEffectMetadata;
    deleteReviewReply$: Observable<({
        review: import("../models/Review.model").ReviewModel;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.MessagesActionTypes.DeleteReviewReplySuccess>) | ({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.MessagesActionTypes.DeleteReviewReplyFail>)> & import("@ngrx/effects").CreateEffectMetadata;
    constructor(actions$: Actions, store$: Store<AppState>, service: IMessagesService);
}
export interface AppState {
    messages: fromReducer.MessagesState;
}
