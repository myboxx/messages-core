import { IStateErrorBase, IStateSuccessBase } from '@boxx/core';
export interface IMessagesStateError extends IStateErrorBase {
    after: 'GET' | 'SET_LEAD_READ' | 'UPSERT_REVIEW' | 'DELETE_REVIEW' | 'UNKNOWN';
}
export interface IMessagesStateSuccess extends IStateSuccessBase {
    after: 'GET' | 'SET_LEAD_READ' | 'UPSERT_REVIEW' | 'DELETE_REVIEW' | 'UNKNOWN';
}
