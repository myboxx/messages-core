/**
 * For any type of message:
 * Lead and Review by now
 */
export interface IMessageBaseProps {
    id: number | string;
    senderName: string;
    message: string;
    createTime: string;
    source: MessageSource;
}
/**
 * Superclass for any type of message:
 * Lead and Review by now
 */
export declare class MessageBaseModel implements IMessageBaseProps {
    constructor(data: IMessageBaseProps);
    id: number | string;
    senderName: string;
    message: string;
    createTime: string;
    source: MessageSource;
}
export declare type MESSAGE_TYPE = 'ALL' | 'READ' | 'NEW';
export declare enum MessageSource {
    EMAIL = 0,
    GMB_MESSAGES = 1,
    GMB_REVIEW = 2,
    CHAT_WEBSITE = 3,
    UNKNOWN = 4
}
