import { ILeadApi, ILeadsApi, ILeadsTotalsApi, IPeriodsApi } from './Leads-api.properties';
import { IMessageBaseProps, MessageBaseModel } from './Message.model';
export interface LeadsModelProps {
    totals: ILeadsTotalsApi;
    periods: IPeriodsApi;
    leads: LeadModel[];
    chart: number[];
}
export declare class LeadsModel implements LeadsModelProps {
    constructor(data: LeadsModelProps);
    totals: ILeadsTotalsApi;
    periods: IPeriodsApi;
    leads: LeadModel[];
    chart: number[];
    static fromApiResponse(data: ILeadsApi): LeadsModel;
    static empty(): LeadsModel;
}
export interface ILeadModelProps extends IMessageBaseProps {
    email: string;
    phone: string;
    formattedDate: string;
    date: string;
    hour: string;
    timeAgo: string;
    leadTimeAgo: string;
    readStatus: boolean;
    shortMessage: string;
    favorite?: boolean;
}
export declare class LeadModel extends MessageBaseModel implements ILeadModelProps {
    email: string;
    phone: string;
    formattedDate: string;
    date: string;
    hour: string;
    timeAgo: string;
    leadTimeAgo: string;
    readStatus: boolean;
    shortMessage: string;
    favorite?: boolean;
    constructor(data: ILeadModelProps);
    static fromApiResponse(data: ILeadApi): LeadModel;
    static empty(): LeadModel;
}
