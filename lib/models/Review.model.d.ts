import { IMessageBaseProps, MessageBaseModel } from './Message.model';
import { IReviewApi, IReviewReplyApi, IReviewsApi } from './Reviews-api.properties';
export interface IReviewsModelProps {
    averageRating: number;
    reviews: ReviewModel[];
    totalReviewCount: number;
}
export declare class ReviewsModel implements IReviewsModelProps {
    constructor(data: IReviewsModelProps);
    averageRating: number;
    reviews: ReviewModel[];
    totalReviewCount: number;
    static fromApiResponse(data: IReviewsApi): ReviewsModel;
    empty(): ReviewsModel;
}
export interface ReviewModelProps extends IMessageBaseProps {
    senderPhotoUrl: string;
    starRating: number;
    updateTime: string;
    reviewReply?: IReviewReplyApi;
}
export declare class ReviewModel extends MessageBaseModel implements ReviewModelProps {
    constructor(data: ReviewModelProps);
    senderPhotoUrl: string;
    starRating: number;
    updateTime: string;
    reviewReply?: IReviewReplyApi;
    static fromApiResponse(data: IReviewApi): ReviewModel;
}
