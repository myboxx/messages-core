export interface ILeadsApi {
    totals: ILeadsTotalsApi;
    periods: IPeriodsApi;
    messages: ILeadApi[];
    chart: number[];
}
export interface ILeadApi {
    id: string;
    name: string;
    message: string;
    mail: string;
    phone: string;
    timestamp: string;
    formatted_date: string;
    date: string;
    hour: string;
    time_ago: string;
    lead_time_ago: string;
    read_status: '0' | '1';
    short_message: string;
    data_name?: string;
    data_message?: string;
    created_at?: string;
    data_email?: string;
    data_phone?: string;
}
export interface IPeriodsApi {
    today: ILeadsPointApi;
    yesterday: ILeadsPointApi;
    seven_days: ILeadsPointApi;
    thirty_days: ILeadsPointApi;
    this_month: ILeadsPointApi;
    last_month: ILeadsPointApi;
    all: ILeadsPointApi;
}
export interface ILeadsPointApi {
    pointStart: number;
    begin: number;
    end: number;
}
export interface ILeadsTotalsApi {
    total: number;
    seen: number;
    new: number;
}
