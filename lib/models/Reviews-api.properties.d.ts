export interface IReviewsApi {
    averageRating: number;
    reviews: IReviewApi[];
    totalReviewCount: number;
}
export interface IReviewApi {
    id: string;
    comment?: string;
    createTime: string;
    reviewer: IReviewerApi;
    reviewReply?: IReviewReplyApi;
    starRating: number;
    updateTime: string;
}
export interface IReviewUpsertApi {
    comment: string;
    updateTime: string;
}
export interface IReviewerApi {
    displayName: string;
    profilePhotoUrl: string;
}
export interface IReviewReplyApi {
    comment: string;
    updateTime: string;
}
