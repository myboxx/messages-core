import { MessageBaseModel } from './Message.model';
export interface IMessagesPageProps {
    stats: {
        leads: {
            error?: any;
            total: number;
            read: number;
            new: number;
        };
        reviews: {
            error?: any;
            averageRating: number;
            totalReviewCount: number;
        };
    };
    messages: MessageBaseModel[];
}
export declare class MessagesPageModel implements IMessagesPageProps {
    stats: {
        leads: {
            error?: any;
            total: number;
            read: number;
            new: number;
        };
        reviews: {
            error?: any;
            averageRating: number;
            totalReviewCount: number;
        };
    };
    messages: MessageBaseModel[];
    constructor(data: IMessagesPageProps);
    static empty(): MessagesPageModel;
}
