import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromActions from './messages.actions';
import * as fromSelector from './messages.selectors';
var MessagesStore = /** @class */ (function () {
    function MessagesStore(store) {
        this.store = store;
    }
    Object.defineProperty(MessagesStore.prototype, "Loading$", {
        get: function () { return this.store.select(fromSelector.getIsLoading); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MessagesStore.prototype, "Error$", {
        get: function () {
            return this.store.select(fromSelector.getError);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MessagesStore.prototype, "Success$", {
        get: function () {
            return this.store.select(fromSelector.getSuccess);
        },
        enumerable: true,
        configurable: true
    });
    MessagesStore.prototype.loadMessagesPage = function (sorting) {
        return this.store.dispatch(fromActions.GetMessagesBeginAction({ sorting: sorting }));
    };
    Object.defineProperty(MessagesStore.prototype, "MessagesPageData$", {
        get: function () {
            return this.store.select(fromSelector.getMessagesPageData);
        },
        enumerable: true,
        configurable: true
    });
    MessagesStore.prototype.filterMessagesByReadStatus = function (criteria) {
        this.store.dispatch(fromActions.FilterMessagesByReadStatusBeginAction({ messageType: criteria }));
    };
    MessagesStore.prototype.filterMessagesBySource = function (sorting, sources) {
        if (sorting === void 0) { sorting = 'ASC'; }
        this.store.dispatch(fromActions.FilterMessagesBySourceBeginAction({ sorting: sorting, sources: sources }));
    };
    Object.defineProperty(MessagesStore.prototype, "FilteredMessages$", {
        get: function () {
            return this.store.select(fromSelector.getFilteredMessages);
        },
        enumerable: true,
        configurable: true
    });
    MessagesStore.prototype.MessageById$ = function (messageId) {
        this.store.dispatch(fromActions.SelectMessageAction({ messageId: messageId }));
        return this.store.select(fromSelector.getMessageById);
    };
    Object.defineProperty(MessagesStore.prototype, "Leads$", {
        get: function () {
            return this.store.select(fromSelector.getLeads);
        },
        enumerable: true,
        configurable: true
    });
    MessagesStore.prototype.setLeadAsRead = function (id) {
        this.store.dispatch(fromActions.SetLeadAsReadBeginAction({ id: id }));
    };
    Object.defineProperty(MessagesStore.prototype, "Reviews$", {
        get: function () {
            return this.store.select(fromSelector.getReviews);
        },
        enumerable: true,
        configurable: true
    });
    MessagesStore.prototype.upsertReviewReply = function (review, comment) {
        this.store.dispatch(fromActions.UpsertReviewReplyBeginAction({ review: review, comment: comment }));
    };
    MessagesStore.prototype.deleteReviewReply = function (review) {
        this.store.dispatch(fromActions.DeleteReviewReplyBeginAction({ review: review }));
    };
    Object.defineProperty(MessagesStore.prototype, "HasBeenFetched$", {
        get: function () {
            return this.store.select(fromSelector.hasBeenFetched);
        },
        enumerable: true,
        configurable: true
    });
    MessagesStore.ctorParameters = function () { return [
        { type: Store }
    ]; };
    MessagesStore = __decorate([
        Injectable()
    ], MessagesStore);
    return MessagesStore;
}());
export { MessagesStore };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVzc2FnZXMuc3RvcmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYm94eC9tZXNzYWdlcy1jb3JlLyIsInNvdXJjZXMiOlsibGliL3N0YXRlL21lc3NhZ2VzLnN0b3JlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFHcEMsT0FBTyxLQUFLLFdBQVcsTUFBTSxvQkFBb0IsQ0FBQztBQUVsRCxPQUFPLEtBQUssWUFBWSxNQUFNLHNCQUFzQixDQUFDO0FBR3JEO0lBQ0ksdUJBQW1CLEtBQXVDO1FBQXZDLFVBQUssR0FBTCxLQUFLLENBQWtDO0lBQUksQ0FBQztJQUUvRCxzQkFBSSxtQ0FBUTthQUFaLGNBQWlCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQzs7O09BQUE7SUFFdkUsc0JBQUksaUNBQU07YUFBVjtZQUNJLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3BELENBQUM7OztPQUFBO0lBRUQsc0JBQUksbUNBQVE7YUFBWjtZQUNJLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3RELENBQUM7OztPQUFBO0lBRUQsd0NBQWdCLEdBQWhCLFVBQWlCLE9BQXdCO1FBQ3JDLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLHNCQUFzQixDQUFDLEVBQUUsT0FBTyxTQUFBLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDaEYsQ0FBQztJQUVELHNCQUFJLDRDQUFpQjthQUFyQjtZQUNJLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFDL0QsQ0FBQzs7O09BQUE7SUFFRCxrREFBMEIsR0FBMUIsVUFBMkIsUUFBc0I7UUFDN0MsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLHFDQUFxQyxDQUFDLEVBQUUsV0FBVyxFQUFFLFFBQVEsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUN0RyxDQUFDO0lBRUQsOENBQXNCLEdBQXRCLFVBQXVCLE9BQStCLEVBQUUsT0FBd0I7UUFBekQsd0JBQUEsRUFBQSxlQUErQjtRQUNsRCxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsaUNBQWlDLENBQUMsRUFBRSxPQUFPLFNBQUEsRUFBRSxPQUFPLFNBQUEsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUM3RixDQUFDO0lBRUQsc0JBQUksNENBQWlCO2FBQXJCO1lBQ0ksT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsbUJBQW1CLENBQUMsQ0FBQztRQUMvRCxDQUFDOzs7T0FBQTtJQUVELG9DQUFZLEdBQVosVUFBYSxTQUFpQjtRQUMxQixJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsbUJBQW1CLENBQUMsRUFBRSxTQUFTLFdBQUEsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNwRSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsQ0FBQztJQUMxRCxDQUFDO0lBRUQsc0JBQUksaUNBQU07YUFBVjtZQUNJLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3BELENBQUM7OztPQUFBO0lBRUQscUNBQWEsR0FBYixVQUFjLEVBQVU7UUFDcEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLHdCQUF3QixDQUFDLEVBQUUsRUFBRSxJQUFBLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDdEUsQ0FBQztJQUVELHNCQUFJLG1DQUFRO2FBQVo7WUFDSSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUN0RCxDQUFDOzs7T0FBQTtJQUVELHlDQUFpQixHQUFqQixVQUFrQixNQUFtQixFQUFFLE9BQWU7UUFDbEQsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLDRCQUE0QixDQUFDLEVBQUUsTUFBTSxRQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDdkYsQ0FBQztJQUVELHlDQUFpQixHQUFqQixVQUFrQixNQUFtQjtRQUNqQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsNEJBQTRCLENBQUMsRUFBRSxNQUFNLFFBQUEsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUM5RSxDQUFDO0lBRUQsc0JBQUksMENBQWU7YUFBbkI7WUFDSSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUMxRCxDQUFDOzs7T0FBQTs7Z0JBM0R5QixLQUFLOztJQUR0QixhQUFhO1FBRHpCLFVBQVUsRUFBRTtPQUNBLGFBQWEsQ0E2RHpCO0lBQUQsb0JBQUM7Q0FBQSxBQTdERCxJQTZEQztTQTdEWSxhQUFhIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgU3RvcmUgfSBmcm9tICdAbmdyeC9zdG9yZSc7XG5pbXBvcnQgeyBNZXNzYWdlU291cmNlLCBNRVNTQUdFX1RZUEUgfSBmcm9tICcuLi9tb2RlbHMvTWVzc2FnZS5tb2RlbCc7XG5pbXBvcnQgeyBSZXZpZXdNb2RlbCB9IGZyb20gJy4uL21vZGVscy9SZXZpZXcubW9kZWwnO1xuaW1wb3J0ICogYXMgZnJvbUFjdGlvbnMgZnJvbSAnLi9tZXNzYWdlcy5hY3Rpb25zJztcbmltcG9ydCAqIGFzIGZyb21SZWR1Y2VyIGZyb20gJy4vbWVzc2FnZXMucmVkdWNlcic7XG5pbXBvcnQgKiBhcyBmcm9tU2VsZWN0b3IgZnJvbSAnLi9tZXNzYWdlcy5zZWxlY3RvcnMnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgTWVzc2FnZXNTdG9yZSB7XG4gICAgY29uc3RydWN0b3IocHVibGljIHN0b3JlOiBTdG9yZTxmcm9tUmVkdWNlci5NZXNzYWdlc1N0YXRlPikgeyB9XG5cbiAgICBnZXQgTG9hZGluZyQoKSB7IHJldHVybiB0aGlzLnN0b3JlLnNlbGVjdChmcm9tU2VsZWN0b3IuZ2V0SXNMb2FkaW5nKTsgfVxuXG4gICAgZ2V0IEVycm9yJCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc3RvcmUuc2VsZWN0KGZyb21TZWxlY3Rvci5nZXRFcnJvcik7XG4gICAgfVxuXG4gICAgZ2V0IFN1Y2Nlc3MkKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5zdG9yZS5zZWxlY3QoZnJvbVNlbGVjdG9yLmdldFN1Y2Nlc3MpO1xuICAgIH1cblxuICAgIGxvYWRNZXNzYWdlc1BhZ2Uoc29ydGluZz86ICdBU0MnIHwgJ0RFU0MnKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnN0b3JlLmRpc3BhdGNoKGZyb21BY3Rpb25zLkdldE1lc3NhZ2VzQmVnaW5BY3Rpb24oeyBzb3J0aW5nIH0pKTtcbiAgICB9XG5cbiAgICBnZXQgTWVzc2FnZXNQYWdlRGF0YSQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnN0b3JlLnNlbGVjdChmcm9tU2VsZWN0b3IuZ2V0TWVzc2FnZXNQYWdlRGF0YSk7XG4gICAgfVxuXG4gICAgZmlsdGVyTWVzc2FnZXNCeVJlYWRTdGF0dXMoY3JpdGVyaWE6IE1FU1NBR0VfVFlQRSkge1xuICAgICAgICB0aGlzLnN0b3JlLmRpc3BhdGNoKGZyb21BY3Rpb25zLkZpbHRlck1lc3NhZ2VzQnlSZWFkU3RhdHVzQmVnaW5BY3Rpb24oeyBtZXNzYWdlVHlwZTogY3JpdGVyaWEgfSkpO1xuICAgIH1cblxuICAgIGZpbHRlck1lc3NhZ2VzQnlTb3VyY2Uoc29ydGluZzogJ0FTQycgfCAnREVTQycgPSAnQVNDJywgc291cmNlczogTWVzc2FnZVNvdXJjZVtdKSB7XG4gICAgICAgIHRoaXMuc3RvcmUuZGlzcGF0Y2goZnJvbUFjdGlvbnMuRmlsdGVyTWVzc2FnZXNCeVNvdXJjZUJlZ2luQWN0aW9uKHsgc29ydGluZywgc291cmNlcyB9KSk7XG4gICAgfVxuXG4gICAgZ2V0IEZpbHRlcmVkTWVzc2FnZXMkKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5zdG9yZS5zZWxlY3QoZnJvbVNlbGVjdG9yLmdldEZpbHRlcmVkTWVzc2FnZXMpO1xuICAgIH1cblxuICAgIE1lc3NhZ2VCeUlkJChtZXNzYWdlSWQ6IG51bWJlcikge1xuICAgICAgICB0aGlzLnN0b3JlLmRpc3BhdGNoKGZyb21BY3Rpb25zLlNlbGVjdE1lc3NhZ2VBY3Rpb24oeyBtZXNzYWdlSWQgfSkpO1xuICAgICAgICByZXR1cm4gdGhpcy5zdG9yZS5zZWxlY3QoZnJvbVNlbGVjdG9yLmdldE1lc3NhZ2VCeUlkKTtcbiAgICB9XG5cbiAgICBnZXQgTGVhZHMkKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5zdG9yZS5zZWxlY3QoZnJvbVNlbGVjdG9yLmdldExlYWRzKTtcbiAgICB9XG5cbiAgICBzZXRMZWFkQXNSZWFkKGlkOiBudW1iZXIpIHtcbiAgICAgICAgdGhpcy5zdG9yZS5kaXNwYXRjaChmcm9tQWN0aW9ucy5TZXRMZWFkQXNSZWFkQmVnaW5BY3Rpb24oeyBpZCB9KSk7XG4gICAgfVxuXG4gICAgZ2V0IFJldmlld3MkKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5zdG9yZS5zZWxlY3QoZnJvbVNlbGVjdG9yLmdldFJldmlld3MpO1xuICAgIH1cblxuICAgIHVwc2VydFJldmlld1JlcGx5KHJldmlldzogUmV2aWV3TW9kZWwsIGNvbW1lbnQ6IHN0cmluZyl7XG4gICAgICAgIHRoaXMuc3RvcmUuZGlzcGF0Y2goZnJvbUFjdGlvbnMuVXBzZXJ0UmV2aWV3UmVwbHlCZWdpbkFjdGlvbih7IHJldmlldywgY29tbWVudCB9KSk7XG4gICAgfVxuXG4gICAgZGVsZXRlUmV2aWV3UmVwbHkocmV2aWV3OiBSZXZpZXdNb2RlbCl7XG4gICAgICAgIHRoaXMuc3RvcmUuZGlzcGF0Y2goZnJvbUFjdGlvbnMuRGVsZXRlUmV2aWV3UmVwbHlCZWdpbkFjdGlvbih7IHJldmlldyB9KSk7XG4gICAgfVxuXG4gICAgZ2V0IEhhc0JlZW5GZXRjaGVkJCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc3RvcmUuc2VsZWN0KGZyb21TZWxlY3Rvci5oYXNCZWVuRmV0Y2hlZCk7XG4gICAgfVxufVxuIl19