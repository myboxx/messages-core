import { __decorate, __read, __spread } from "tslib";
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { MessageItemComponent } from './components/message-item/message-item.component';
import { MESSAGES_REPOSITORY } from './repositories/IMessages.repository';
import { MessagesRepository } from './repositories/messages.repository';
import { MESSAGES_SERVICE } from './services/IMessages.service';
import { MessagesService } from './services/messages.service';
import { MessagesEffects } from './state/messages.effects';
import { messagesReducer } from './state/messages.reducer';
import { MessagesStore } from './state/messages.store';
var MessagesCoreModule = /** @class */ (function () {
    function MessagesCoreModule() {
    }
    MessagesCoreModule_1 = MessagesCoreModule;
    MessagesCoreModule.forRoot = function (config) {
        return {
            ngModule: MessagesCoreModule_1,
            providers: __spread([
                { provide: MESSAGES_SERVICE, useClass: MessagesService },
                { provide: MESSAGES_REPOSITORY, useClass: MessagesRepository }
            ], config.providers, [
                MessagesStore
            ])
        };
    };
    var MessagesCoreModule_1;
    MessagesCoreModule = MessagesCoreModule_1 = __decorate([
        NgModule({
            declarations: [MessageItemComponent],
            imports: [
                HttpClientModule,
                StoreModule.forFeature('messages', messagesReducer),
                EffectsModule.forFeature([MessagesEffects]),
                CommonModule,
                FormsModule,
                IonicModule
            ],
            exports: [
                CommonModule,
                FormsModule,
                MessageItemComponent,
            ]
        })
    ], MessagesCoreModule);
    return MessagesCoreModule;
}());
export { MessagesCoreModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVzc2FnZXMtY29yZS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYm94eC9tZXNzYWdlcy1jb3JlLyIsInNvdXJjZXMiOlsibGliL21lc3NhZ2VzLWNvcmUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDeEQsT0FBTyxFQUF1QixRQUFRLEVBQVksTUFBTSxlQUFlLENBQUM7QUFDeEUsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzdDLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUM3QyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzlDLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFDMUMsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sa0RBQWtELENBQUM7QUFDeEYsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFDMUUsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFDeEUsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDaEUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQzlELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUMzRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDM0QsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBc0J2RDtJQUFBO0lBWUMsQ0FBQzsyQkFaVyxrQkFBa0I7SUFDcEIsMEJBQU8sR0FBZCxVQUFlLE1BQThCO1FBQ3pDLE9BQU87WUFDSCxRQUFRLEVBQUUsb0JBQWtCO1lBQzVCLFNBQVM7Z0JBQ0wsRUFBRSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsUUFBUSxFQUFFLGVBQWUsRUFBRTtnQkFDeEQsRUFBRSxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsUUFBUSxFQUFFLGtCQUFrQixFQUFDO2VBQzFELE1BQU0sQ0FBQyxTQUFTO2dCQUNuQixhQUFhO2NBQ2hCO1NBQ0osQ0FBQztJQUNOLENBQUM7O0lBWFEsa0JBQWtCO1FBaEI5QixRQUFRLENBQUM7WUFDTixZQUFZLEVBQUUsQ0FBQyxvQkFBb0IsQ0FBQztZQUNwQyxPQUFPLEVBQUU7Z0JBQ0wsZ0JBQWdCO2dCQUNoQixXQUFXLENBQUMsVUFBVSxDQUFDLFVBQVUsRUFBRSxlQUFlLENBQUM7Z0JBQ25ELGFBQWEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQztnQkFDM0MsWUFBWTtnQkFDWixXQUFXO2dCQUNYLFdBQVc7YUFDZDtZQUNELE9BQU8sRUFBRTtnQkFDTCxZQUFZO2dCQUNaLFdBQVc7Z0JBQ1gsb0JBQW9CO2FBQ3ZCO1NBQ0osQ0FBQztPQUNXLGtCQUFrQixDQVk3QjtJQUFELHlCQUFDO0NBQUEsQUFaRixJQVlFO1NBWlcsa0JBQWtCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7IEh0dHBDbGllbnRNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBNb2R1bGVXaXRoUHJvdmlkZXJzLCBOZ01vZHVsZSwgUHJvdmlkZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZvcm1zTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHsgSW9uaWNNb2R1bGUgfSBmcm9tICdAaW9uaWMvYW5ndWxhcic7XG5pbXBvcnQgeyBFZmZlY3RzTW9kdWxlIH0gZnJvbSAnQG5ncngvZWZmZWN0cyc7XG5pbXBvcnQgeyBTdG9yZU1vZHVsZSB9IGZyb20gJ0BuZ3J4L3N0b3JlJztcbmltcG9ydCB7IE1lc3NhZ2VJdGVtQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL21lc3NhZ2UtaXRlbS9tZXNzYWdlLWl0ZW0uY29tcG9uZW50JztcbmltcG9ydCB7IE1FU1NBR0VTX1JFUE9TSVRPUlkgfSBmcm9tICcuL3JlcG9zaXRvcmllcy9JTWVzc2FnZXMucmVwb3NpdG9yeSc7XG5pbXBvcnQgeyBNZXNzYWdlc1JlcG9zaXRvcnkgfSBmcm9tICcuL3JlcG9zaXRvcmllcy9tZXNzYWdlcy5yZXBvc2l0b3J5JztcbmltcG9ydCB7IE1FU1NBR0VTX1NFUlZJQ0UgfSBmcm9tICcuL3NlcnZpY2VzL0lNZXNzYWdlcy5zZXJ2aWNlJztcbmltcG9ydCB7IE1lc3NhZ2VzU2VydmljZSB9IGZyb20gJy4vc2VydmljZXMvbWVzc2FnZXMuc2VydmljZSc7XG5pbXBvcnQgeyBNZXNzYWdlc0VmZmVjdHMgfSBmcm9tICcuL3N0YXRlL21lc3NhZ2VzLmVmZmVjdHMnO1xuaW1wb3J0IHsgbWVzc2FnZXNSZWR1Y2VyIH0gZnJvbSAnLi9zdGF0ZS9tZXNzYWdlcy5yZWR1Y2VyJztcbmltcG9ydCB7IE1lc3NhZ2VzU3RvcmUgfSBmcm9tICcuL3N0YXRlL21lc3NhZ2VzLnN0b3JlJztcblxuaW50ZXJmYWNlIE1vZHVsZU9wdGlvbnNJbnRlcmZhY2Uge1xuICAgIHByb3ZpZGVyczogUHJvdmlkZXJbXTtcbn1cblxuQE5nTW9kdWxlKHtcbiAgICBkZWNsYXJhdGlvbnM6IFtNZXNzYWdlSXRlbUNvbXBvbmVudF0sXG4gICAgaW1wb3J0czogW1xuICAgICAgICBIdHRwQ2xpZW50TW9kdWxlLFxuICAgICAgICBTdG9yZU1vZHVsZS5mb3JGZWF0dXJlKCdtZXNzYWdlcycsIG1lc3NhZ2VzUmVkdWNlciksXG4gICAgICAgIEVmZmVjdHNNb2R1bGUuZm9yRmVhdHVyZShbTWVzc2FnZXNFZmZlY3RzXSksXG4gICAgICAgIENvbW1vbk1vZHVsZSxcbiAgICAgICAgRm9ybXNNb2R1bGUsXG4gICAgICAgIElvbmljTW9kdWxlXG4gICAgXSxcbiAgICBleHBvcnRzOiBbXG4gICAgICAgIENvbW1vbk1vZHVsZSxcbiAgICAgICAgRm9ybXNNb2R1bGUsXG4gICAgICAgIE1lc3NhZ2VJdGVtQ29tcG9uZW50LFxuICAgIF1cbn0pXG5leHBvcnQgY2xhc3MgTWVzc2FnZXNDb3JlTW9kdWxlIHtcbiAgICBzdGF0aWMgZm9yUm9vdChjb25maWc6IE1vZHVsZU9wdGlvbnNJbnRlcmZhY2UpOiBNb2R1bGVXaXRoUHJvdmlkZXJzPE1lc3NhZ2VzQ29yZU1vZHVsZT4ge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgbmdNb2R1bGU6IE1lc3NhZ2VzQ29yZU1vZHVsZSxcbiAgICAgICAgICAgIHByb3ZpZGVyczogW1xuICAgICAgICAgICAgICAgIHsgcHJvdmlkZTogTUVTU0FHRVNfU0VSVklDRSwgdXNlQ2xhc3M6IE1lc3NhZ2VzU2VydmljZSB9LFxuICAgICAgICAgICAgICAgIHsgcHJvdmlkZTogTUVTU0FHRVNfUkVQT1NJVE9SWSwgdXNlQ2xhc3M6IE1lc3NhZ2VzUmVwb3NpdG9yeX0sXG4gICAgICAgICAgICAgICAgLi4uY29uZmlnLnByb3ZpZGVycyxcbiAgICAgICAgICAgICAgICBNZXNzYWdlc1N0b3JlXG4gICAgICAgICAgICBdXG4gICAgICAgIH07XG4gICAgfVxuIH1cbiJdfQ==