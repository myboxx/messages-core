(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/common'), require('@angular/common/http'), require('@angular/core'), require('@angular/forms'), require('@ionic/angular'), require('@ngrx/effects'), require('@ngrx/store'), require('@boxx/core'), require('rxjs'), require('rxjs/operators')) :
    typeof define === 'function' && define.amd ? define('@boxx/messages-core', ['exports', '@angular/common', '@angular/common/http', '@angular/core', '@angular/forms', '@ionic/angular', '@ngrx/effects', '@ngrx/store', '@boxx/core', 'rxjs', 'rxjs/operators'], factory) :
    (global = global || self, factory((global.boxx = global.boxx || {}, global.boxx['messages-core'] = {}), global.ng.common, global.ng.common.http, global.ng.core, global.ng.forms, global['ionic-angular'], global['ngrx-effects'], global['ngrx-store'], global['boxx-core'], global.rxjs, global.rxjs.operators));
}(this, (function (exports, common, http, core, forms, angular, effects, store, core$1, rxjs, operators) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation.

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }

    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    }

    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    function __createBinding(o, m, k, k2) {
        if (k2 === undefined) k2 = k;
        o[k2] = m[k];
    }

    function __exportStar(m, exports) {
        for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) exports[p] = m[p];
    }

    function __values(o) {
        var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
        if (m) return m.call(o);
        if (o && typeof o.length === "number") return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
        throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    }

    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    };

    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
    }

    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }

    function __asyncValues(o) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
    }

    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
        return cooked;
    };

    function __importStar(mod) {
        if (mod && mod.__esModule) return mod;
        var result = {};
        if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
        result.default = mod;
        return result;
    }

    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }

    function __classPrivateFieldGet(receiver, privateMap) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to get private field on non-instance");
        }
        return privateMap.get(receiver);
    }

    function __classPrivateFieldSet(receiver, privateMap, value) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to set private field on non-instance");
        }
        privateMap.set(receiver, value);
        return value;
    }

    var MessageItemComponent = /** @class */ (function () {
        function MessageItemComponent() {
            this.detail = true;
        }
        MessageItemComponent.prototype.ngOnInit = function () {
        };
        __decorate([
            core.Input()
        ], MessageItemComponent.prototype, "detail", void 0);
        MessageItemComponent = __decorate([
            core.Component({
                selector: 'boxx-message-item',
                template: "<div class=\"message-item-row\">\n    <div class=\"message-item-row-content\">\n        <div class=\"message-item-row-content-top\">\n            <div class=\"top-icon-container\">\n                <ng-content select=\"div[slot=top-left]\"></ng-content>\n            </div>\n            <ion-row>\n                <ion-col class=\"col-left\">\n                    <div>\n                        <ng-content select=\"div[slot=top-center]\"></ng-content>\n                    </div>\n                </ion-col>\n                <ion-col class=\"col-right\">\n                    <div>\n                        <ng-content select=\"div[slot=top-right]\"></ng-content>\n                    </div>\n                </ion-col>\n            </ion-row>\n        </div>\n\n        <ion-row class=\"bottom-row\">\n            <ion-col class=\"left-col\" size=\"6\">\n                <div class=\"bottom-left\">\n                    <ng-content select=\"ion-label[slot=bottom-start]\"></ng-content>\n                </div>\n            </ion-col>\n            <ion-col class=\"right-col\" size=\"6\">\n                <div class=\"right\">\n                    <div class=\"top\">\n                        <ng-content select=\"div[slot=bottom-end-top]\"></ng-content>\n                    </div>\n                    <div class=\"bottom\">\n                        <ng-content select=\"div[slot=bottom-end-bottom]\"></ng-content>\n                    </div>\n                </div>\n            </ion-col>\n        </ion-row>\n        <ng-content></ng-content>\n    </div>\n\n    <div class=\"message-detail-icon\">\n        <ion-icon name=\"chevron-forward-outline\" [hidden]=\"!detail\"></ion-icon>\n    </div>\n</div>",
                styles: [":host .message-item-row{display:flex;justify-content:space-between;align-items:center;padding:16px 0 16px 16px}:host .message-item-row ion-col{padding:0}:host .message-item-row-content{width:100%}:host .message-item-row-content-top{display:flex;justify-content:space-between;margin-bottom:8px}:host .message-item-row-content-top .top-icon-container{font-size:x-large;width:36px;height:36px;background:#d3d3d3;border-radius:8px}:host .message-item-row-content-top ion-row{width:100%}:host .message-item-row-content-top .col-left{white-space:normal;padding-left:8px}:host .message-item-row-content-top .col-right{padding-top:3px;font-size:smaller;text-align:right}:host .bottom-row .left-col{margin-top:8px}:host .bottom-row .right-col .right{display:flex;height:100%;flex-wrap:wrap;align-content:space-between;justify-content:space-evenly}:host .bottom-row .right-col .right .top{width:100%;display:flex;justify-content:flex-end}:host .bottom-row .right-col .right .bottom{width:100%;text-align:right}:host .message-detail-icon{min-width:32px}:host .message-detail-icon ion-icon{margin-left:8px;font-size:x-large}"]
            })
        ], MessageItemComponent);
        return MessageItemComponent;
    }());

    var MESSAGES_REPOSITORY = new core.InjectionToken('messagesRepository');

    var MessagesRepository = /** @class */ (function () {
        function MessagesRepository(appSettings, httpClient) {
            this.appSettings = appSettings;
            this.httpClient = httpClient;
        }
        MessagesRepository.prototype.getLeads = function () {
            return this.httpClient.get(this.getBaseUrl() + "/leads");
        };
        MessagesRepository.prototype.setLeadAsRead = function (id) {
            var data = { lead_id: id };
            var urlSearchParams = new URLSearchParams();
            Object.keys(data).forEach(function (key, i) {
                urlSearchParams.append(key, data[key]);
            });
            var body = urlSearchParams.toString();
            return this.httpClient.post(this.getBaseUrl() + "/leads/mark_message_as_read", body);
        };
        MessagesRepository.prototype.getReviews = function () {
            return this.httpClient.get(this.getBaseUrl() + "/gmb/reviews");
        };
        MessagesRepository.prototype.getReview = function (reviewId) {
            return this.httpClient.get(this.getBaseUrl() + "/gmb/review/" + reviewId);
        };
        MessagesRepository.prototype.upsertReviewReply = function (reviewId, comment) {
            var params = new http.HttpParams();
            params = params.append('comment', comment);
            return this.httpClient.post(this.getBaseUrl() + "/gmb/upsertReview/" + reviewId, params.toString());
        };
        MessagesRepository.prototype.deleteReviewReply = function (reviewId) {
            return this.httpClient.delete(this.getBaseUrl() + "/gmb/deleteReview/" + reviewId);
        };
        MessagesRepository.prototype.getBaseUrl = function () {
            return this.appSettings.baseUrl() + "/api/" + this.appSettings.instance() + "/v1";
        };
        MessagesRepository.ctorParameters = function () { return [
            { type: core$1.AbstractAppConfigService, decorators: [{ type: core.Inject, args: [core$1.APP_CONFIG_SERVICE,] }] },
            { type: http.HttpClient }
        ]; };
        MessagesRepository = __decorate([
            core.Injectable(),
            __param(0, core.Inject(core$1.APP_CONFIG_SERVICE))
        ], MessagesRepository);
        return MessagesRepository;
    }());

    var MESSAGES_SERVICE = new core.InjectionToken('messagesService');

    /**
     * Superclass for any type of message:
     * Lead and Review by now
     */
    var MessageBaseModel = /** @class */ (function () {
        function MessageBaseModel(data) {
            this.id = data.id;
            this.senderName = data.senderName;
            this.message = data.message;
            this.createTime = data.createTime;
            this.source = data.source;
        }
        return MessageBaseModel;
    }());

    (function (MessageSource) {
        MessageSource[MessageSource["EMAIL"] = 0] = "EMAIL";
        MessageSource[MessageSource["GMB_MESSAGES"] = 1] = "GMB_MESSAGES";
        MessageSource[MessageSource["GMB_REVIEW"] = 2] = "GMB_REVIEW";
        MessageSource[MessageSource["CHAT_WEBSITE"] = 3] = "CHAT_WEBSITE";
        MessageSource[MessageSource["UNKNOWN"] = 4] = "UNKNOWN";
    })(exports.MessageSource || (exports.MessageSource = {}));

    var LeadsModel = /** @class */ (function () {
        function LeadsModel(data) {
            this.totals = data.totals;
            this.periods = data.periods;
            this.leads = data.leads;
            this.chart = data.chart;
        }
        LeadsModel.fromApiResponse = function (data) {
            var leads = data.messages.map(function (lead) { return LeadModel.fromApiResponse(lead); });
            return new LeadsModel({
                totals: data.totals,
                periods: data.periods,
                leads: leads,
                chart: data.chart
            });
        };
        LeadsModel.empty = function () {
            return new LeadsModel({
                totals: {
                    total: 0,
                    seen: 0,
                    new: 0
                },
                periods: null,
                leads: [],
                chart: []
            });
        };
        return LeadsModel;
    }());
    var LeadModel = /** @class */ (function (_super) {
        __extends(LeadModel, _super);
        function LeadModel(data) {
            var _this = _super.call(this, {
                id: data.id,
                senderName: data.senderName,
                message: data.message,
                createTime: data.createTime,
                source: data.source
            }) || this;
            _this.email = data.email;
            _this.phone = data.phone;
            _this.formattedDate = data.formattedDate;
            _this.date = data.date;
            _this.hour = data.hour;
            _this.timeAgo = data.timeAgo;
            _this.leadTimeAgo = data.leadTimeAgo;
            _this.readStatus = data.readStatus;
            var shortMsg = (data.shortMessage || data.message.substring(0, 30));
            _this.shortMessage = shortMsg.length >= 30 ? shortMsg.concat('...') : shortMsg;
            _this.source = data.source;
            return _this;
        }
        LeadModel.fromApiResponse = function (data) {
            return new LeadModel({
                id: +data.id,
                senderName: data.name || data.data_name,
                message: data.message || data.data_message,
                email: data.mail || data.data_email,
                phone: data.phone || data.data_phone,
                createTime: data.timestamp,
                formattedDate: data.formatted_date,
                date: data.date || data.created_at,
                hour: data.hour,
                timeAgo: data.time_ago,
                leadTimeAgo: data.lead_time_ago,
                readStatus: data.read_status === '1',
                shortMessage: data.short_message,
                source: exports.MessageSource.EMAIL
            });
        };
        LeadModel.empty = function () {
            return new LeadModel({
                id: null,
                senderName: '',
                createTime: null,
                message: '',
                email: null,
                phone: null,
                formattedDate: null,
                date: null,
                hour: null,
                timeAgo: null,
                leadTimeAgo: null,
                readStatus: null,
                shortMessage: null,
                source: exports.MessageSource.UNKNOWN,
                favorite: false
            });
        };
        return LeadModel;
    }(MessageBaseModel));

    var MessagesPageModel = /** @class */ (function () {
        function MessagesPageModel(data) {
            this.stats = data.stats;
            this.messages = data.messages;
        }
        MessagesPageModel.empty = function () {
            return new MessagesPageModel({
                stats: {
                    leads: {
                        total: 0,
                        read: 0,
                        new: 0
                    },
                    reviews: {
                        averageRating: 0,
                        totalReviewCount: 0
                    }
                },
                messages: [],
            });
        };
        return MessagesPageModel;
    }());

    var ReviewsModel = /** @class */ (function () {
        function ReviewsModel(data) {
            this.averageRating = data.averageRating;
            this.reviews = data.reviews;
            this.totalReviewCount = data.totalReviewCount;
        }
        ReviewsModel.fromApiResponse = function (data) {
            var reviewList = (!Array.isArray(data.reviews) ? [] : data.reviews).map(function (review) { return ReviewModel.fromApiResponse({
                id: review.id,
                comment: review.comment,
                createTime: review.createTime,
                reviewer: review.reviewer,
                starRating: review.starRating,
                updateTime: review.updateTime,
                reviewReply: review.reviewReply
            }); });
            return new ReviewsModel({
                averageRating: data.averageRating || 0,
                reviews: reviewList,
                totalReviewCount: data.totalReviewCount || 0
            });
        };
        ReviewsModel.prototype.empty = function () {
            return new ReviewsModel({
                averageRating: null,
                reviews: null,
                totalReviewCount: null
            });
        };
        return ReviewsModel;
    }());
    var ReviewModel = /** @class */ (function (_super) {
        __extends(ReviewModel, _super);
        function ReviewModel(data) {
            var _this = _super.call(this, {
                id: data.id,
                senderName: data.senderName,
                message: data.message,
                createTime: data.createTime,
                source: data.source
            }) || this;
            _this.senderPhotoUrl = data.senderPhotoUrl;
            _this.starRating = data.starRating;
            _this.updateTime = data.updateTime;
            _this.reviewReply = data.reviewReply;
            return _this;
        }
        ReviewModel.fromApiResponse = function (data) {
            return new ReviewModel({
                // base class properties:
                id: data.id,
                senderName: data.reviewer.displayName,
                message: data.comment,
                createTime: data.createTime,
                source: exports.MessageSource.GMB_REVIEW,
                // own (extended) properties:
                senderPhotoUrl: data.reviewer.profilePhotoUrl,
                starRating: data.starRating,
                updateTime: data.updateTime,
                reviewReply: data.reviewReply
            });
        };
        return ReviewModel;
    }(MessageBaseModel));

    var MessagesService = /** @class */ (function () {
        function MessagesService(repository) {
            this.repository = repository;
        }
        MessagesService.prototype.getMessages = function (sorting) {
            var leadsError;
            var ReviewsError;
            return rxjs.forkJoin([
                this.getLeads().pipe(operators.catchError(function (err) { leadsError = err; return rxjs.of(null); })),
                this.getReviews().pipe(operators.catchError(function (err) { ReviewsError = err; return rxjs.of(null); })),
            ]).pipe(operators.map(function (_a) {
                var _b = __read(_a, 2), leadsData = _b[0], reviewsData = _b[1];
                return new MessagesPageModel({
                    stats: {
                        leads: {
                            error: leadsError,
                            total: leadsError ? 0 : leadsData.totals.total,
                            read: leadsError ? 0 : leadsData.totals.seen,
                            new: leadsError ? 0 : leadsData.totals.new
                        },
                        reviews: {
                            error: ReviewsError,
                            averageRating: ReviewsError ? 0 : reviewsData.averageRating,
                            totalReviewCount: ReviewsError ? 0 : reviewsData.totalReviewCount
                        }
                    },
                    messages: __spread((leadsError ? [] : __spread(leadsData.leads)), (ReviewsError ? [] : __spread(reviewsData.reviews))).sort(function (a, b) {
                        return (sorting === 'ASC' ?
                            b.createTime.localeCompare(a.createTime) :
                            a.createTime.localeCompare(b.createTime));
                    }),
                });
            }));
        };
        MessagesService.prototype.getLeads = function () {
            return this.repository.getLeads().pipe(operators.map(function (response) {
                return LeadsModel.fromApiResponse(response.data);
            }));
        };
        MessagesService.prototype.setLeadAsRead = function (id) {
            return this.repository.setLeadAsRead(id).pipe(operators.map(function (response) {
                return LeadModel.fromApiResponse(response.data);
            }));
        };
        MessagesService.prototype.getReviews = function () {
            return this.repository.getReviews().pipe(operators.map(function (response) {
                return ReviewsModel.fromApiResponse(response.data);
            }));
        };
        MessagesService.prototype.getReview = function (reviewId) {
            return this.repository.getReview(reviewId).pipe(operators.map(function (response) {
                return ReviewModel.fromApiResponse(response.data);
            }));
        };
        MessagesService.prototype.upsertReviewReply = function (review, comment) {
            return this.repository.upsertReviewReply(review.id, comment).pipe(operators.map(function (response) {
                return new ReviewModel({
                    id: review.id,
                    senderName: review.senderName,
                    message: review.message,
                    createTime: review.createTime,
                    source: review.source,
                    senderPhotoUrl: review.senderPhotoUrl,
                    starRating: review.starRating,
                    updateTime: review.updateTime,
                    reviewReply: {
                        comment: comment,
                        updateTime: response.data.updateTime,
                    }
                });
            }));
        };
        MessagesService.prototype.deleteReviewReply = function (review) {
            return this.repository.deleteReviewReply(review.id).pipe(operators.map(function (response) {
                if (response.status === 'success') {
                    return new ReviewModel({
                        id: review.id,
                        senderName: review.senderName,
                        message: review.message,
                        createTime: review.createTime,
                        source: review.source,
                        senderPhotoUrl: review.senderPhotoUrl,
                        starRating: review.starRating,
                        updateTime: review.updateTime
                    });
                }
                else {
                    throw Error(response.message || 'Unknown error');
                }
            }));
        };
        MessagesService.ctorParameters = function () { return [
            { type: undefined, decorators: [{ type: core.Inject, args: [MESSAGES_REPOSITORY,] }] }
        ]; };
        MessagesService.ɵprov = core.ɵɵdefineInjectable({ factory: function MessagesService_Factory() { return new MessagesService(core.ɵɵinject(MESSAGES_REPOSITORY)); }, token: MessagesService, providedIn: "root" });
        MessagesService = __decorate([
            core.Injectable({
                providedIn: 'root'
            }),
            __param(0, core.Inject(MESSAGES_REPOSITORY))
        ], MessagesService);
        return MessagesService;
    }());


    (function (MessagesActionTypes) {
        MessagesActionTypes["GetMessagesBegin"] = "[Messages] Get Messages begin";
        MessagesActionTypes["GetMessagesSuccess"] = "[Messages] Get Messages success";
        MessagesActionTypes["GetMessagesFail"] = "[Messages] Get Messages failure";
        MessagesActionTypes["SetLeadAsReadBegin"] = "[Messages] Set Lead as Read begin";
        MessagesActionTypes["SetLeadAsReadSuccess"] = "[Messages] Set Messages as Read success";
        MessagesActionTypes["SetLeadAsReadFail"] = "[Messages] Set Messages as Read failure";
        MessagesActionTypes["UpsertReviewReplyBegin"] = "[Messages] Upsert Review Reply begin";
        MessagesActionTypes["UpsertReviewReplySuccess"] = "[Messages] Upsert Review Reply success";
        MessagesActionTypes["UpsertReviewReplyFail"] = "[Messages] Upsert Review Reply failure";
        MessagesActionTypes["DeleteReviewReplyBegin"] = "[Messages] Delete Review Reply begin";
        MessagesActionTypes["DeleteReviewReplySuccess"] = "[Messages] Delete Review Reply success";
        MessagesActionTypes["DeleteReviewReplyFail"] = "[Messages] Delete Review Reply failure";
        MessagesActionTypes["FilterMessagesByReadStatusBegin"] = "[Messages] Filter message by Read Status begin";
        MessagesActionTypes["FilterMessagesByReadStatusSuccess"] = "[Messages] Filter message success";
        MessagesActionTypes["FilterMessagesBySourceBegin"] = "[Messages] Filter message by Source begin";
        MessagesActionTypes["FilterMessagesSourceSuccess"] = "[Messages] Filter message by Source success";
        MessagesActionTypes["SortMessagesByDateBegin"] = "[Messages] Sort message by Date begin";
        MessagesActionTypes["SortMessagesByDateSuccess"] = "[Messages] Sort message by Date success";
        MessagesActionTypes["SelectMessage"] = "[Messages] Select message";
    })(exports.MessagesActionTypes || (exports.MessagesActionTypes = {}));
    // GET Messages from remote API
    var GetMessagesBeginAction = store.createAction(exports.MessagesActionTypes.GetMessagesBegin, store.props());
    var GetMessagesSuccessAction = store.createAction(exports.MessagesActionTypes.GetMessagesSuccess, store.props());
    var GetMessagesFailAction = store.createAction(exports.MessagesActionTypes.GetMessagesFail, store.props());
    // SET AS READ
    var SetLeadAsReadBeginAction = store.createAction(exports.MessagesActionTypes.SetLeadAsReadBegin, store.props());
    var SetLeadAsReadSuccessAction = store.createAction(exports.MessagesActionTypes.SetLeadAsReadSuccess, store.props());
    var SetLeadAsReadFailAction = store.createAction(exports.MessagesActionTypes.SetLeadAsReadFail, store.props());
    var UpsertReviewReplyBeginAction = store.createAction(exports.MessagesActionTypes.UpsertReviewReplyBegin, store.props());
    var UpsertReviewReplySuccessAction = store.createAction(exports.MessagesActionTypes.UpsertReviewReplySuccess, store.props());
    var UpsertReviewReplyFaliAction = store.createAction(exports.MessagesActionTypes.UpsertReviewReplyFail, store.props());
    var DeleteReviewReplyBeginAction = store.createAction(exports.MessagesActionTypes.DeleteReviewReplyBegin, store.props());
    var DeleteReviewReplySuccessAction = store.createAction(exports.MessagesActionTypes.DeleteReviewReplySuccess, store.props());
    var DeleteReviewReplyFaliAction = store.createAction(exports.MessagesActionTypes.DeleteReviewReplyFail, store.props());
    // FILTERING
    var FilterMessagesByReadStatusBeginAction = store.createAction(exports.MessagesActionTypes.FilterMessagesByReadStatusBegin, store.props());
    var FilterMessagesByReadStatusSuccessAction = store.createAction(exports.MessagesActionTypes.FilterMessagesByReadStatusSuccess, store.props());
    var FilterMessagesBySourceBeginAction = store.createAction(exports.MessagesActionTypes.FilterMessagesBySourceBegin, store.props());
    var FilterMessagesBySourceSuccessAction = store.createAction(exports.MessagesActionTypes.FilterMessagesSourceSuccess, store.props());
    var SelectMessageAction = store.createAction(exports.MessagesActionTypes.SelectMessage, store.props());

    var MessagesEffects = /** @class */ (function () {
        function MessagesEffects(actions$, store$, service) {
            var _this = this;
            this.actions$ = actions$;
            this.store$ = store$;
            this.service = service;
            this.load$ = effects.createEffect(function () { return _this.actions$.pipe(effects.ofType(exports.MessagesActionTypes.GetMessagesBegin), operators.switchMap(function (action) {
                return _this.service.getMessages(action.sorting).pipe(operators.map(function (data) { return GetMessagesSuccessAction({ data: data }); }), operators.catchError(function (error) {
                    console.error('Couldn\'t get messages', error);
                    return rxjs.of(GetMessagesFailAction({ errors: error }));
                }));
            })); });
            this.setAsRead$ = effects.createEffect(function () { return _this.actions$.pipe(effects.ofType(exports.MessagesActionTypes.SetLeadAsReadBegin), operators.switchMap(function (action) {
                return _this.service.setLeadAsRead(action.id).pipe(operators.map(function (message) { return SetLeadAsReadSuccessAction({ message: message }); }), operators.catchError(function (error) {
                    console.error('Couldn\'t set message as read', error);
                    return rxjs.of(SetLeadAsReadFailAction({ errors: error }));
                }));
            })); });
            this.filter$ = effects.createEffect(function () { return _this.actions$.pipe(effects.ofType(exports.MessagesActionTypes.FilterMessagesByReadStatusBegin), operators.withLatestFrom(_this.store$), operators.switchMap(function (_a) {
                var _b = __read(_a, 2), action = _b[0], store = _b[1];
                var messageType = action.messageType;
                var messageList = store.messages.pageData.messages.filter(function (item) { return store.messages.filteredSources.includes(item.source); });
                messageList = messageType === 'ALL' ? messageList :
                    messageList.filter(function (item) {
                        if (messageType === 'READ' && item.readStatus === undefined) {
                            return true;
                        }
                        return item.readStatus === (messageType === 'READ' ? true : false);
                    });
                return rxjs.of(FilterMessagesByReadStatusSuccessAction({ messageList: messageList }));
            })); });
            this.sortAndFilter$ = effects.createEffect(function () { return _this.actions$.pipe(effects.ofType(exports.MessagesActionTypes.FilterMessagesBySourceBegin), operators.withLatestFrom(_this.store$), operators.switchMap(function (_a) {
                var _b = __read(_a, 2), action = _b[0], store = _b[1];
                var sources = action.sources;
                var sorting = action.sorting;
                var sortedMessages = store.messages.pageData.messages
                    .map(function (m) { return m; }) // <-- because pageData.messages is READ ONLY!!
                    .sort(function (a, b) {
                    return (sorting === 'ASC' ?
                        b.createTime.localeCompare(a.createTime) :
                        a.createTime.localeCompare(b.createTime));
                });
                var filteredMessages = sortedMessages.filter(function (item) { return sources.includes(item.source); });
                return rxjs.of(FilterMessagesBySourceSuccessAction({
                    sorting: sorting,
                    sources: sources,
                    filteredMessages: filteredMessages
                }));
            })); });
            this.upsertReviewReply$ = effects.createEffect(function () { return _this.actions$.pipe(effects.ofType(UpsertReviewReplyBeginAction), operators.switchMap(function (action) {
                return _this.service.upsertReviewReply(action.review, action.comment).pipe(operators.map(function (review) { return UpsertReviewReplySuccessAction({ review: review }); }), operators.catchError(function (error) {
                    console.error('Couldn\'t upsert review reply', error);
                    return rxjs.of(UpsertReviewReplyFaliAction({ errors: error }));
                }));
            })); });
            this.deleteReviewReply$ = effects.createEffect(function () { return _this.actions$.pipe(effects.ofType(DeleteReviewReplyBeginAction), operators.switchMap(function (action) {
                return _this.service.deleteReviewReply(action.review).pipe(operators.map(function (review) { return DeleteReviewReplySuccessAction({ review: review }); }), operators.catchError(function (error) {
                    console.error('Couldn\'t delete review reply', error);
                    return rxjs.of(DeleteReviewReplyFaliAction({ errors: error }));
                }));
            })); });
        }
        MessagesEffects.ctorParameters = function () { return [
            { type: effects.Actions },
            { type: store.Store },
            { type: undefined, decorators: [{ type: core.Inject, args: [MESSAGES_SERVICE,] }] }
        ]; };
        MessagesEffects = __decorate([
            core.Injectable(),
            __param(2, core.Inject(MESSAGES_SERVICE))
        ], MessagesEffects);
        return MessagesEffects;
    }());

    var initialState = {
        isLoading: false,
        hasBeenFetched: false,
        pageData: MessagesPageModel.empty(),
        filteredSources: [],
        filteredItems: [],
        sorting: 'ASC',
        selectedId: null,
        error: null,
        success: null
    };
    var ɵ0 = function (state) { return (__assign(__assign({}, state), { error: null, success: null, isLoading: true })); }, ɵ1 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, hasBeenFetched: true, pageData: __assign(__assign({}, state.pageData), { stats: action.data.stats, messages: action.data.messages }), error: null, success: { after: getAfterActionType(action.type) } })); }, ɵ2 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, hasBeenFetched: true, pageData: __assign(__assign({}, state.pageData), { messages: state.pageData.messages.map(function (m) {
                return m.id === action.review.id ? action.review : m;
            }) }), error: null, success: { after: getAfterActionType(action.type) } })); }, ɵ3 = function (state, action) { return (__assign(__assign({}, state), { pageData: __assign(__assign({}, state.pageData), { stats: __assign(__assign({}, state.pageData.stats), { leads: __assign(__assign({}, state.pageData.stats.leads), { read: state.pageData.stats.leads.read + 1, new: state.pageData.stats.leads.new - 1 }) }), messages: __spread((function (ml) {
                var tmp = __spread(ml);
                var idx = ml.findIndex(function (m) { return m.id === action.message.id; });
                if (idx !== -1) {
                    tmp.splice(idx, 1, action.message);
                }
                return tmp;
            })(state.pageData.messages)) }), error: null, success: { after: getAfterActionType(action.type) } })); }, ɵ4 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, error: { after: getAfterActionType(action.type), error: action.errors } })); }, ɵ5 = function (state, action) { return (__assign(__assign({}, state), { error: { after: getAfterActionType(action.type), error: action.errors } })); }, ɵ6 = function (state, action) { return (__assign(__assign({}, state), { filteredItems: action.messageList, success: null })); }, ɵ7 = function (state, action) { return (__assign(__assign({}, state), { sorting: action.sorting, filteredSources: action.sources, filteredItems: action.filteredMessages, success: null })); }, ɵ8 = function (state, action) { return (__assign(__assign({}, state), { selectedId: action.messageId, success: null })); };
    var reducer = store.createReducer(initialState, 
    // On Begin Actions
    store.on(GetMessagesBeginAction, UpsertReviewReplyBeginAction, DeleteReviewReplyBeginAction, ɵ0), 
    // ON Success Actions
    store.on(GetMessagesSuccessAction, ɵ1), store.on(UpsertReviewReplySuccessAction, DeleteReviewReplySuccessAction, ɵ2), store.on(SetLeadAsReadSuccessAction, ɵ3), 
    // ON Fail Actions
    store.on(GetMessagesFailAction, UpsertReviewReplyFaliAction, DeleteReviewReplyFaliAction, ɵ4), store.on(SetLeadAsReadFailAction, ɵ5), 
    // FILTER
    store.on(FilterMessagesByReadStatusSuccessAction, ɵ6), store.on(FilterMessagesBySourceSuccessAction, ɵ7), 
    // SELECT
    store.on(SelectMessageAction, ɵ8));
    function getAfterActionType(type) {
        var action;
        switch (type) {
            case exports.MessagesActionTypes.GetMessagesSuccess:
            case exports.MessagesActionTypes.GetMessagesFail:
                action = 'GET';
                break;
            case exports.MessagesActionTypes.SetLeadAsReadSuccess:
            case exports.MessagesActionTypes.SetLeadAsReadFail:
                action = 'SET_LEAD_READ';
                break;
            case exports.MessagesActionTypes.UpsertReviewReplySuccess:
            case exports.MessagesActionTypes.UpsertReviewReplyFail:
                action = 'UPSERT_REVIEW';
                break;
            case exports.MessagesActionTypes.DeleteReviewReplyFail:
            case exports.MessagesActionTypes.DeleteReviewReplySuccess:
                action = 'DELETE_REVIEW';
                break;
            default:
                action = 'UNKNOWN';
        }
        return action;
    }
    function messagesReducer(state, action) {
        return reducer(state, action);
    }

    var getMessagesState = store.createFeatureSelector('messages');
    var stateGetPageData = function (state) { return state.pageData; };
    var stateGetIsLoading = function (state) { return state.isLoading; };
    var stateGetMessages = function (state) { return state.pageData.messages; };
    var stateGetLeads = function (state) { return state.pageData.messages.filter(function (m) { return m.source === exports.MessageSource.EMAIL; }); };
    var stateGetReviews = function (state) { return state.pageData.messages.filter(function (m) { return m.source === exports.MessageSource.GMB_REVIEW; }); };
    var stateGetFilteredItems = function (state) { return state.filteredItems; };
    var ɵ0$1 = function (state) { return state; };
    var getMessagesPageState = store.createSelector(getMessagesState, ɵ0$1);
    var getMessagesPageData = store.createSelector(getMessagesState, stateGetPageData);
    var getMessages = store.createSelector(getMessagesPageState, stateGetMessages);
    var getLeads = store.createSelector(getMessagesPageState, stateGetLeads);
    var getReviews = store.createSelector(getMessagesPageState, stateGetReviews);
    var getIsLoading = store.createSelector(getMessagesPageState, stateGetIsLoading);
    var ɵ1$1 = function (state) { return state.error; };
    var getError = store.createSelector(getMessagesPageState, ɵ1$1);
    var ɵ2$1 = function (state) { return state.success; };
    var getSuccess = store.createSelector(getMessagesPageState, ɵ2$1);
    var getFilteredMessages = store.createSelector(getMessagesPageState, stateGetFilteredItems);
    var ɵ3$1 = function (state) { return state.pageData.messages.filter(function (m) { return m.id === state.selectedId; })[0]; };
    var getMessageById = store.createSelector(getMessagesPageState, ɵ3$1);
    var ɵ4$1 = function (state) { return state.hasBeenFetched; };
    var hasBeenFetched = store.createSelector(getMessagesPageState, ɵ4$1);

    var MessagesStore = /** @class */ (function () {
        function MessagesStore(store) {
            this.store = store;
        }
        Object.defineProperty(MessagesStore.prototype, "Loading$", {
            get: function () { return this.store.select(getIsLoading); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MessagesStore.prototype, "Error$", {
            get: function () {
                return this.store.select(getError);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MessagesStore.prototype, "Success$", {
            get: function () {
                return this.store.select(getSuccess);
            },
            enumerable: true,
            configurable: true
        });
        MessagesStore.prototype.loadMessagesPage = function (sorting) {
            return this.store.dispatch(GetMessagesBeginAction({ sorting: sorting }));
        };
        Object.defineProperty(MessagesStore.prototype, "MessagesPageData$", {
            get: function () {
                return this.store.select(getMessagesPageData);
            },
            enumerable: true,
            configurable: true
        });
        MessagesStore.prototype.filterMessagesByReadStatus = function (criteria) {
            this.store.dispatch(FilterMessagesByReadStatusBeginAction({ messageType: criteria }));
        };
        MessagesStore.prototype.filterMessagesBySource = function (sorting, sources) {
            if (sorting === void 0) { sorting = 'ASC'; }
            this.store.dispatch(FilterMessagesBySourceBeginAction({ sorting: sorting, sources: sources }));
        };
        Object.defineProperty(MessagesStore.prototype, "FilteredMessages$", {
            get: function () {
                return this.store.select(getFilteredMessages);
            },
            enumerable: true,
            configurable: true
        });
        MessagesStore.prototype.MessageById$ = function (messageId) {
            this.store.dispatch(SelectMessageAction({ messageId: messageId }));
            return this.store.select(getMessageById);
        };
        Object.defineProperty(MessagesStore.prototype, "Leads$", {
            get: function () {
                return this.store.select(getLeads);
            },
            enumerable: true,
            configurable: true
        });
        MessagesStore.prototype.setLeadAsRead = function (id) {
            this.store.dispatch(SetLeadAsReadBeginAction({ id: id }));
        };
        Object.defineProperty(MessagesStore.prototype, "Reviews$", {
            get: function () {
                return this.store.select(getReviews);
            },
            enumerable: true,
            configurable: true
        });
        MessagesStore.prototype.upsertReviewReply = function (review, comment) {
            this.store.dispatch(UpsertReviewReplyBeginAction({ review: review, comment: comment }));
        };
        MessagesStore.prototype.deleteReviewReply = function (review) {
            this.store.dispatch(DeleteReviewReplyBeginAction({ review: review }));
        };
        Object.defineProperty(MessagesStore.prototype, "HasBeenFetched$", {
            get: function () {
                return this.store.select(hasBeenFetched);
            },
            enumerable: true,
            configurable: true
        });
        MessagesStore.ctorParameters = function () { return [
            { type: store.Store }
        ]; };
        MessagesStore = __decorate([
            core.Injectable()
        ], MessagesStore);
        return MessagesStore;
    }());

    var MessagesCoreModule = /** @class */ (function () {
        function MessagesCoreModule() {
        }
        MessagesCoreModule_1 = MessagesCoreModule;
        MessagesCoreModule.forRoot = function (config) {
            return {
                ngModule: MessagesCoreModule_1,
                providers: __spread([
                    { provide: MESSAGES_SERVICE, useClass: MessagesService },
                    { provide: MESSAGES_REPOSITORY, useClass: MessagesRepository }
                ], config.providers, [
                    MessagesStore
                ])
            };
        };
        var MessagesCoreModule_1;
        MessagesCoreModule = MessagesCoreModule_1 = __decorate([
            core.NgModule({
                declarations: [MessageItemComponent],
                imports: [
                    http.HttpClientModule,
                    store.StoreModule.forFeature('messages', messagesReducer),
                    effects.EffectsModule.forFeature([MessagesEffects]),
                    common.CommonModule,
                    forms.FormsModule,
                    angular.IonicModule
                ],
                exports: [
                    common.CommonModule,
                    forms.FormsModule,
                    MessageItemComponent,
                ]
            })
        ], MessagesCoreModule);
        return MessagesCoreModule;
    }());

    exports.DeleteReviewReplyBeginAction = DeleteReviewReplyBeginAction;
    exports.DeleteReviewReplyFaliAction = DeleteReviewReplyFaliAction;
    exports.DeleteReviewReplySuccessAction = DeleteReviewReplySuccessAction;
    exports.FilterMessagesByReadStatusBeginAction = FilterMessagesByReadStatusBeginAction;
    exports.FilterMessagesByReadStatusSuccessAction = FilterMessagesByReadStatusSuccessAction;
    exports.FilterMessagesBySourceBeginAction = FilterMessagesBySourceBeginAction;
    exports.FilterMessagesBySourceSuccessAction = FilterMessagesBySourceSuccessAction;
    exports.GetMessagesBeginAction = GetMessagesBeginAction;
    exports.GetMessagesFailAction = GetMessagesFailAction;
    exports.GetMessagesSuccessAction = GetMessagesSuccessAction;
    exports.LeadModel = LeadModel;
    exports.LeadsModel = LeadsModel;
    exports.MESSAGES_REPOSITORY = MESSAGES_REPOSITORY;
    exports.MESSAGES_SERVICE = MESSAGES_SERVICE;
    exports.MessageBaseModel = MessageBaseModel;
    exports.MessagesCoreModule = MessagesCoreModule;
    exports.MessagesEffects = MessagesEffects;
    exports.MessagesPageModel = MessagesPageModel;
    exports.MessagesRepository = MessagesRepository;
    exports.MessagesService = MessagesService;
    exports.MessagesStore = MessagesStore;
    exports.ReviewModel = ReviewModel;
    exports.ReviewsModel = ReviewsModel;
    exports.SelectMessageAction = SelectMessageAction;
    exports.SetLeadAsReadBeginAction = SetLeadAsReadBeginAction;
    exports.SetLeadAsReadFailAction = SetLeadAsReadFailAction;
    exports.SetLeadAsReadSuccessAction = SetLeadAsReadSuccessAction;
    exports.UpsertReviewReplyBeginAction = UpsertReviewReplyBeginAction;
    exports.UpsertReviewReplyFaliAction = UpsertReviewReplyFaliAction;
    exports.UpsertReviewReplySuccessAction = UpsertReviewReplySuccessAction;
    exports.getError = getError;
    exports.getFilteredMessages = getFilteredMessages;
    exports.getIsLoading = getIsLoading;
    exports.getLeads = getLeads;
    exports.getMessageById = getMessageById;
    exports.getMessages = getMessages;
    exports.getMessagesPageData = getMessagesPageData;
    exports.getMessagesPageState = getMessagesPageState;
    exports.getMessagesState = getMessagesState;
    exports.getReviews = getReviews;
    exports.getSuccess = getSuccess;
    exports.hasBeenFetched = hasBeenFetched;
    exports.initialState = initialState;
    exports.messagesReducer = messagesReducer;
    exports.stateGetFilteredItems = stateGetFilteredItems;
    exports.stateGetIsLoading = stateGetIsLoading;
    exports.stateGetLeads = stateGetLeads;
    exports.stateGetMessages = stateGetMessages;
    exports.stateGetPageData = stateGetPageData;
    exports.stateGetReviews = stateGetReviews;
    exports.ɵ0 = ɵ0$1;
    exports.ɵ1 = ɵ1$1;
    exports.ɵ2 = ɵ2$1;
    exports.ɵ3 = ɵ3$1;
    exports.ɵ4 = ɵ4$1;
    exports.ɵa = MessageItemComponent;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=boxx-messages-core.umd.js.map
